\documentclass[12pt]{report}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage[left=1in, right=1in]{geometry}
%\usepackage[margin=1in]{geometry}
%\geometry{a4paper}
\usepackage{graphicx}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage[parfill]{parskip}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{bbm}
\usepackage{listings}
\usepackage{hyperref}
\usepackage{setspace}
\usepackage{comment}
\usepackage[]{algorithm2e}

\lstset{
  language=Matlab,  
  numbers=left,
  breaklines=true 
}

\setlength{\parindent}{0cm}
\parskip = \baselineskip

\newcommand{\bigO}{\ensuremath{\mathcal{O}}}% big-O notation/symbol

% tikz package for diagrams
\usepackage{tikz}
\usetikzlibrary{decorations.markings}
\usetikzlibrary{fit,positioning,shapes.geometric,arrows}
\newcommand\independent{\protect\mathpalette{\protect\independenT}{\perp}}
\def\independenT#1#2{\mathrel{\rlap{$#1#2$}\mkern2mu{#1#2}}}

\title{Loan Default Prediction: Classifying Clients for Creating Optimal Loan Portfolios}

\author{Satyajeet Pal\\\\ Professor Mengdi Wang\\ Princeton University\\Operations Research and Financial Engineering\\Senior Thesis Fall Progress Report}

\date{\today}

\begin{document}

\maketitle

\newpage

This paper represents my own work in accordance with University regulations. 
%I hereby declare that I am the sole author of this thesis.

%I authorize Princeton University to lend this thesis to other institutions or individuals for the purpose of scholarly research.

%Satyajeet Pal

%I further authorize Princeton University to reproduce this thesis by photocopying or by other means, in total or in part, at the request of other institutions or individuals for the purpose of scholarly research.

Satyajeet Pal

\section{Abstract}

Loans are a ubiquitous part of a capitalist economy. Current methods of evaluating potential loans are dated and often require underwriters to use basic credit scores (which may be inadequate due to the bad or no credit history of most micro-loan borrowers) and simple formulas to evaluate credit risk. This thesis will attempt to create a better algorithm to predict a borrower's risk for default based on an applicants' profile, using various statistical tools to identify those borrowers who are good credit risks. 

\newpage

\tableofcontents

%\listoffigures

\doublespacing

\newpage

\chapter{Introduction}

Loans are a big part of a successful capitalist economy. They give individuals the opportunity to bootstrap their own startup businesses, buy homes and cars, and use credit cards. Reliable credit scoring methods are invaluable from the perspective of financial institutions. They allow these institutions to evaluate loan applicants and intelligently accept new clients. 

Individually, defaults on loans can lead to potentially large losses for banks. When part of a large portfolio of loans owned by financial institutions, they can be disastrous. Strings of loan failures can cause banks to lose large portions of their capital and lead to their failure. Recent financial and debt crises, such as the 2008 subprime mortgage crisis in the US, have made banks and other financial institutions more wary of giving out loans. However, in order to continue acquiring and serving customers, these institutions must be smarter about deciding which loans to grant rather than simply granting fewer loans. Thus, in recent years credit risk assessment and management has become an area of bigger focus. 

With improved loan evaluation, companies can not only give out loans to a higher proportion of qualified applicants but can also provide lower rates to the borrowers that they've classified as low risk.
 

\section{Motivation}

This topic and thesis were inspired by ongoing discussion and collaboration with Professor Wang and previous discussions with the founders at Earnest, a startup in the lending space. Professor Wang already had an interest in loan related problems. There are also various startups involved with micro lending or merit-based lending using different approaches to determine credit-worthiness. The end goal of many startups, such as Earnest, is to use statistics and machine learning to more intelligently gauge whether or not to grant an individual a loan. Using more sophisticated algorithms, they can better determine who to grant loans to and what kinds of loan to grant. 

From a mathematical perspective, the problem of taking risk into account when classifying loans and determining optimal loan portfolios is a challenging and interesting one. These companies are using real, and often limited, capital to fund these loans and must determine what level of risk they are willing to take when issuing loans. As a result, a simple risk neutral classification of applications can be misleading or dangerous when applied to real loan portfolios. We are motivated to be take risk into account in the optimization problem. 

We are also encouraged to explore the possibility of correlations between individual loan applications. This takes into account situations, similar to the housing crisis, where multiple subprime loans all defaulted together because there were high correlations between them. In these cases, the risk of default may be higher than if we make the assumption of uncorrelated loans.


\newpage

\chapter{Overview of Thesis}

\section{Objective}
The main objective of this thesis is to help loan providers make optimal decisions when giving out loans based on applicants' profiles. I will look at various methods of classifying loans according to different criteria. First, I explore method of risk-neutral classification where risk is not taken into account when classifying loan applications. Then, I explore classification where the objective is to create a risk-sensitive optimization vector. 

Before we continue, we will define some terms to make the rest of the discussion clearer. We will use the term "institution" to refer to the financial entity that issues loans to applicants. We will use the term "applicant" to refer to the individual (or entity) applying for a loan from the institution. 

\section{Data}

The clearest and most available source of data is the Kaggle Loan Default Competition (\url{https://www.kaggle.com/c/loan-default-prediction}).

The Kaggle dataset is a large dataset corresponding to a set of financial transactions by individuals. It is  completely anonymized dataset and has been standardized and de-trended [Kaggle]. The data in the training set is sorted by date. In order to remove any time-dimensionality, we randomize the order of the data. 

The attributes of the borrowers is multi-dimensional and thus, part of the challenge will be deciding which features can be used for prediction. The data is a combination of categorical, discrete, and continuous variables. The dataset has approximately 200,000 observations and 800 features. Unfortunately, the attributes are unlabeled. While this doesn't necessarily affect our algorithms, it obscures the analysis because there is no economic or financial context to the features we find important.

In addition to the attributes applicants, each observation in the dataset has a record of whether or not a default occurred for that application. Whenever there was a default, the loss was also recorded with a value between 0 and 100. For example, if the loss value was 25, then that means only 75 was returned from the loan (where the loan amount is normalized to 100). 

This dataset consists of both a training and testing set. Having these separate sets will be valuable if I pursue the classification model and decision tree aspect of the loan default problem. These datasets will also be useful if I decide to instead focus more on the optimal learning problem of discovery of new borrowers. 

While the dataset is large, there are holes in the data, so we use a strategy to fill in the gaps. Because there are so many features, it is valuable to do some feature selection in order to reduce the time required to build the classification model. 

An important point to be careful of when doing the classification is that typical credit scoring data is heavily imbalanced. That is, there are much fewer defaults than non-defaults in the population of loan applications. Thus, conventional classification techniques may bias heavily toward non-default and seem accurate just because there is only a small proportion of default loan applications. This was discussed in the literature review. 

In order to find more data, I have been trying to reach out to loan providers to see if they'd be willing to share their datasets with me. Two specific startups come to mind that are trying to replace traditional banks as loan-givers. They are Earnest (\url{https://www.meetearnest.com/}) and Lending Club (\url{https://www.lendingclub.com/}). The value of this data is that the features may be labeled. It will also provide real-world data for me to test my classification model. 

In addition, if I work with the real-world data, there is more likely to be insufficient data because the companies are relatively young. Thus, the optimal learning problem of adaptive discovery will be more relevant to them. I can use the classification model in combination with my optimal learning algorithms to help suggest which loans these young companies should accept given their limited datasets. 

\section{Classification}
Classification is a two-fold process. First, a binary classier identifies whether a loan is determined to be a default or non-default. The first step gives us a probability of default for an application. Using a particular threshold, yet to be discovered, we can classify each loan application as likely to result in a default or not likely to result in a default. Second, we take the loans that we feel are likely to result in a default and predict the potential loss. By establishing this classification process, a loan provider can used the train model to take any new loan application and probabilistically determine two things: whether or not the loan will default and the value of expected default.  
 
\subsection{Risk-Neutral}

At its core, the classification problem is an optimization problem. A risk neutral binary classifier tries to minimize the error or loss between the classifier and the truth. We will explore the best technique for finding the probability of default for loan applications. This classifier does not take risk into account.  There is also a related and interesting regression problem of predicting the expected default amount (on a range between 0 and 100\%) for applicants for whom a default is predicted.  

\subsection{Risk-Sensitive}

Since classification is an optimization problem, we can take a risk-aware approach to classifying the loan applications. This is the branch of optimization that uses as the optimization function rather than a typical risk-neutral loss function. This makes the classification more robust against tail loss. 

\newpage 

\chapter{Literature Review}

\section{Risk-Neutral Classification}

\subsection{Probability of Default Models} 

First, we look at the models used for Probability of Default (PD) classification. Two basic techniques that were referenced time and again were Logistic Regression (LR) and Linear Discriminant Analysis (LDA). They are considered to be "relatively simple and explainable" \cite{Shi} and are often used as a benchmark for classification models to evaluate credit risk. In fact, those two classification models are considered to be the most commonly used credit scoring techniques. They end up being very competitive when compared to more complex techniques \cite{Brown}. This suggests that most credit-scoring data sets are "weakly non-linear" \cite{Baesens}.

\subsection{Support Vector Machines} 

Support Vector Machines are often used for classification and for credit scoring models. Two different papers proposed classification models based on support vector machines and gave insight on how to improve them for the credit scoring application. One was a model for feature-weighted support vector machines \cite{Shi} while the other is a fuzzy support vector machine \cite{Fuzzy}. The feature-weighted support vector machine builds on standard SVMs by assigning different weights to different input features. This paper found that RF-FWSVM (random forest-feature weighted svm) had "high classification accuracy and good stability for practical credit scoring applications" \cite{Shi}. Thus, it is worth exploring these specialized SVMs to improve the credit scoring model. 

Besides SVMs, there are some other techniques to explore based off the literature. Random Forests and Gradient Boosting were both explored in \cite{Lessmann} and \cite{Brown} and scored well for PD classification. These techniques were also used in many of the Kaggle competition submissions and are worth pursuing because they were common to some of the winning entries. 

\subsection{Stochastic Gradient Boosting}

The winning entry by Guocong Song used a Gradient Boosting Classifier. This is a promising technique for both risk-neutral and risk-sensitive classification because it allows for the optimization of some arbitrary differentiable loss function \cite{sklearn-gbc}. 

Two papers by Jerome Friedman describe the Gradient Boosting Classifier implemented in the sklearn Python package. Friedman develops the methodology and applications of the gradient boosting machine \cite{Friedman1}. In it, he describes various popular loss criteria and their applications. The mathematical definition of the Gradient Boosting algorithm is clearly described and helps us understand the usage of the Python sklearn package that implements the GB algorithm. 

More importantly, the paper mathematically defines the "deviance" loss function used by the default GB classifier. This loss function is a measure of measure of the lack of fit to the data in a logistic regression model. The textbook "The Elements of Statistical Learning" \cite{ElementsStatisticalLearning} corroborates this "deviance" loss function as being the quantity "$-2 * \mbox{the log-likelihood}$". These mathematical definitions of the GB classifier will be useful when trying to determine a risk-aware loss function. 

The paper also provides experience to describe performance of the algorithm dependent on the parameters of the GB classifier (as used in sklearn). 

\subsection{Classification Effectiveness}

When analyzing the effectiveness of the classification, I must be careful of the fact that credit scoring data is usually imbalanced with many more "good" applicants than "bad" applicants (in danger of default). It is suggested that a combination of the F-measure and weighted accuracy be used to score my classification models. Literature suggests that a good way to deal with imbalanced data (when using Random Forests) is to use Weighted or Balanced Random Forests \cite{RandomForestImbalanced}. 

Further exploration and lit review must be performed for the second part of the classification which is finding the expected loss given default. This part of the problem is primarily a regression problem. 

\section{Risk-Sensitive Optimization for Classification} 

Literature on risk averse optimization functions generally focus on portfolio optimization and similar applications. For our purposes, we are looking for a loss function that is risk averse so that we can use our classification techniques to minimize it and fit a model that minimizes the downside risk when determining the default probability of loan applicants. According to a talk by Garud Iyengar of Columbia on Optimization with Risk Measures \cite{ColumbiaRisk}, some popular constraints on risk are $\alpha$-quartile Value at Risk (VaR) and Conditional Value at Risk (CVaR). The Conditional Value at Risk is sometimes also commonly referred to as Expected Shortfall. Expected Shortfall or CVaR is a spectral risk measure that is given as a weighted average of outcomes where bad outcomes are typically included with larger weights. This is relevant in the situation of finding a risk-sensitive loss function for credit risk classification. We want to penalize bad outcomes when it comes to misclassifying credit risk (particularly understating the true probability of default) more to reduce overall credit risk. Iyengar describes what VaR and CVaR are defined as:

\[ \mbox{VaR}_\alpha(f(x)) = \inf \{\beta : \Pr(f(x)\geq \beta)) \leq 1-\alpha\}\]
\[ \mbox{CVaR}_\alpha(f(x)) = \int_{\alpha}^{1} \mbox{VaR}_{\alpha}(f(x)) \]

According to Rockafeller \cite{Rockafeller}, of the shortfalls of VaR is that it doesn't adequately measure the extent of the losses beyond the value at risk. It can't really distinguish the extent of losses that can be had past the amount at VaR. The VaR only gives the "lowest bound for losses in the tail of the loss distribution and has a bias toward optimism instead of the conservatism that ought to prevail in risk management."

The better measure is the CVaR. Takeda \cite{Takeda} and Tsyurmasto \cite{Tsyurmasto} provide methods of incorporating CVaR and VaR into Support Vector Machines. They formulate a version of SVMs as a Conditional Value at Risk minimization. I will use these formulations and apply them to the loan default classification problem at hand. 

Kashima further explores the use of CVaR for "Risk-sensitive learning" \cite{Kashima1} and a model for using it with gradient boosting as the optimization approach. The terminology and formulation used by Kashima is very helpful for me when I ultimately formulate my model for risk-sensitive or risk-sensitive classification. 


\newpage

\chapter{Mathematical Model}

Our classification problem of determining the likelihood of default of a borrower based on his attributes is a classic function estimation or "predictive learning problem". Using the mathematical model of Friedman \cite{Friedman1}, we can say we are trying to predict a "response" $y$ given a set of "explanatory" variables $x = \{ x_1, \dots, x_n \}$. In our case, the explanatory variables $x$ are the feature set that we choose for the classification. For our classification, we are trying to find an approximation $F$ of the function $F^*(x)$ that maps $x$ to $y$ and minimizes the expected value of a loss function $L(y, F(x))$ on the joint distribution of $(y,x)$. Again, as described in Friedman\cite{Friedman1}, our classification function is: 

\[ F^* = \arg \min_F E_{y,x} L(y, F(x)) = \arg \min_F E_x [E_y (L(y, F(x))) | x]\]


\section{Risk-Neutral Classification}

For risk-neutral classification (applicable to most classification problems) where we are not accounting for risk when determining an error or loss function for the classifier, some frequently used loss functions $L(y, F)$ are squared error, $(y-F)^2$, absolute error, $|y-F|$, and negative binomial log likelihood, $-2 * log(1+e^{-2yF})$. For the gradient boosted classifier we are using, the default loss function (and the one used most often with gradient boosted trees) is 'deviance' which is the negative binomial log likelihood. 

The method of classification we use for risk-neutral classification is Friedman's Gradient Boosted Machine implemented in the sklearn.ensemble.GradientBoostedClassifier package. 

The Gradient Boosting algorithm builds an additive model in a "forward stage-wise fashion" \cite{sklearn-gbc}. It works with an arbitrary loss function, such as the 'deviance' negative binomial log likelihood function discussed earlier. In each stage of the model, one regression tree is fit on the negative gradient of the binomial deviance loss function. A single regression tree is used because ours is a binary classification. 

The following figure illustrates the algorithm used in Gradient Boost. 

\begin{algorithm}[H]
$F_0 = \arg \min_{\rho} \sum^N_{i=1} L (y_i, \rho)$\;
 	\For{m = 1 to M}{
		$\hat{y_i} = - \big[\frac{\partial L(y_i, F(x_i))}{\partial F(x_i)} \big]$, $i=1,N$\;
		Fit a regression model, h(x), predicting $\hat{y_i}$ from the covariates $x_i$\;
		$\rho_m = \arg \min_p \sum^N_{i=1} L(y_i, F_{m-1} (x_i) + \rho h(x_i))$\;
		$F_m(x) \leftarrow F_{m-1}(x) + \rho_m h(x)$
  		
 	}
 \caption{Gradient Boost Algorithm}
\end{algorithm}

This model creates the classification model and is completely risk-neutral because the loss function doesn't take risk into account. It simply tries to optimize the binomial log likelihood in a stage-wise fashion. 

\newpage

\section{Risk-Sensitive Optimization}

The primary benefit of using cost-sensitive learning is that we aim to minimize the expected cost of misclassification rather than the probability of misclassification. This is suitable in the case that costs are variable throughout the data. 

However, this is still sometimes insufficient in the case of loan classification because a cost-sensitive approach doesn't "aggressively suppress the occurrence of huge costs" \cite{Kashima1}. Thus, for our more applicable risk-sensitive approach, we use the risk metric of expected shortfall or conditional value-at-risk. Our classification algorithm will minimize this expected shortfall as the objective function. Let us define expected shortfall, or value-at-risk. In the loan default prediction model, we will define the expected shortfall $\phi_\beta^D(\theta)$ with respect to the hypothesis $h$ and data distribution $D$ \cite{Kashima1} as: 

\[ \phi_{\beta}^D(\theta) = \frac{1}{1-\beta}   E_D [I(c(x, h(\theta)) \geq \alpha^D_\beta(\theta))c(x, h(\theta))]\]

This value indicates the expected cost above a certain value-at-risk, or the "expectation of the top $100(1-\beta)\%$ costs given a constant $0 \leq \beta \leq 1$. 

Here, we see the term $\alpha^D_\beta(\theta)$, the value-at-risk (VaR) where the value is:

\[ \alpha^D_\beta(\theta) = \min { \alpha \in  \mathbb{R}} |  E_D[I(c(x, h(\theta)) \geq \alpha)] \leq 1-\beta \] 

Here, $I$ is an indicator function that is the value 1 when the argument is true and 0 when the argument is false. 

We can rewrite $\phi_{\beta}^D(\theta)$ because the CVaR is just the expected costs surpassing the VaR:

\[ \phi_{\beta}^D(\theta) = \alpha^D_\beta(\theta) + \frac{1}{1-\beta}  E_D {[c(x, h(\theta))-  \alpha^D_\beta(\theta)]}^+ \] 

where the function ${[x]}^+$ returns $x$ if positive or $0$ if negative. 

Now, we want to use an algorithm that optimizes $\theta$, the model parameters for our risk-sensitive classification model. 

We also slightly alter the model to use the training examples $E$ instead of $D$, the truth that is unknown. 

\begin{equation} \label{cvar_data}
\phi_{\beta}^E(\theta) = \alpha^E_\beta(\theta) + \frac{1}{(1-\beta)N} \sum_{i=1}^N {[c(x, h(\theta))-  \alpha^E_\beta(\theta)]}^+ 
\end{equation}

\begin{equation} \label{var_data}
\alpha^E_\beta(\theta) = \min { \alpha \in  \mathbb{R}} |  \frac{1}{N} \sum_{i=1}^N [I(c(x, h(\theta)) \geq \alpha)] \leq 1-\beta
\end{equation} 

We can assume that the value at risk $\alpha^E_\beta(\theta)$, for a given $E$ and $\beta$, is a known constant that we'll call $\bar{\alpha}$ so we only have to minimize the second term in \eqref{cvar_data}. The term we want to minimize then is the following:

\begin{equation} \label{minimize_c}
\bar{C}^E_{\bar{\alpha}} := \frac{1}{N} \sum_{i=1}^N {[c(x^{(i)}, h(\theta)) - \bar{\alpha}]}^+
\end{equation}

We have algorithms to find $\theta$ that minimize \eqref{minimize_c}. After finding this local minimized shortfall for the given $\bar{\alpha}$, we fix $\theta$, and find the new VaR \eqref{var_data} for that $\theta$. Using this understanding, \cite{Kashima1} developed a risk-sensitive meta-learning algorithm called MetaRisk that minimizes the conditional value-at-risk USING already existing cost-sensitive learners. This MetaRisk algorithm is in the figure below. 

\begin{algorithm}[H]
[Step 1:] Set $\bar{\alpha} := 0$\;
[Step 2:] For $\bar{\alpha}$, find $\theta' = \arg \min_\theta \bar{C}^E_{\bar{\alpha}}(\theta)$, and set $\theta = \theta'$\;
[Step 3:] For this new $\theta$, find the empirical VaR $\alpha^E_\beta(\theta)$ and let $\bar{\alpha} := \alpha^E_\beta(\theta)$\;
[Step 4:] Continue steps 2 and 3 until we converge on the estimator of the expected shortfall: $F^E_\beta(\theta, \bar{\alpha})$\;
 \caption{MetaRisk algorithm}
\end{algorithm}

We can "recycle" existing cost-sensitive learners and make them risk-sensitive by using reweighted costs in each iteration of the MetaRisk algorithm. The cost in each iteration can just be the amount by which the value-at-risk is exceeded for each alternative loan. 

\chapter{Results}

\section{Summary of Project Work}

Thus far, I have completed most of the problem formulation for my thesis. This includes analyzing the Kaggle dataset and understanding the winning submissions. I have adapted these winning suggestions in my code for the risk-neutral classification. 

A significant portion of my time was spent reviewing literature relevant to the classification problem and specifically the loan default classification problem. Much of this literature led me on tangents, but it was necessary for understanding the problem as a whole. 

I have also made significant progress on understanding and writing the mathematical model for risk-neutral classification of credit default. 

\section{Risk-Neutral Classification}

I modified one of the winning entries for the Kaggle competition for the risk-neutral classification. The dataset includes a train and test set; however, the test set does not contain the true loss amounts. Thus, we could use the test set to evaluate the effectiveness of our classification.

Instead, we broke up the training set into a smaller training set and a smaller test set. That way, we could train on the smaller training set and then test the classifier on the smaller test set. When we ran the Gradient Boosting Classifier on the small training set with 3000 estimators, it took about one hour to train. The resulting mean accuracy using the small test set was 

\[ console output: score =  0.9884 \]

In Figure 5.1, you can see the relative feature importance of the top 10 features as determined by the gradient boosting classifier. There were a total of 435 features used for the classification. 

\begin{figure}[h]
  \centering
    \includegraphics[width=1\textwidth]{feature_importance}
  \caption{Feature Importance}

\end{figure}

\newpage

\par In Figure 5.2 and 5.3, you can see the action of the Gradient Boosting Classifier and the stage-wise progression of the algorithm. We can see that by the end of the 3000 estimators, the Out-of-Bag error doesn't change by much and the training loss is not increasing much. This justifies our use of approximately 3000 estimators for the best possible classifier. 

\begin{figure}[h]
  \centering
    \includegraphics[width=0.4\textwidth]{GradientBegin}
  \caption{Beginning of Gradient Boosting Classifier}
\end{figure}

\begin{figure}[h]
  \centering
    \includegraphics[width=0.4\textwidth]{GradientEnd}
  \caption{End of Gradient Boosting Classifier}
\end{figure}

\newpage

\chapter{Conclusion}
\section{Future Work}

\newpage

\singlespacing

\begin{thebibliography}{100} % 100 is a random guess of the total number of references

\bibitem{CCC} Carter, Chris, and Jason Catlett. "Assessing Credit Card Applications Using Machine Learning." IEEE Expert 2, no. 3 (September 1987): 71-79. doi:10.1109/MEX.1987.4307093.
\bibitem{Huang} Huang, Cheng-Lung, Mu-Chen Chen, and Chieh-Jen Wang. "Credit Scoring with a Data Mining Approach Based on Support Vector Machines." Expert Systems with Applications 33, no. 4 (November 2007): 847-56. doi:10.1016/j.eswa.2006.07.007.
\bibitem{Ogler} Orgler, Yair E. "A Credit Scoring Model for Commercial Loans." Journal of Money, Credit and Banking 2, no. 4 (November 1, 1970): 435?45. doi:10.2307/1991095.
\bibitem{Shi} Shi, Jian, Shu-you Zhang, and Le-miao Qiu. "Credit Scoring by Feature-Weighted Support Vector Machines." Journal of Zhejiang University SCIENCE C 14, no. 3 (March 1, 2013): 197-204. doi:10.1631/jzus.C1200205.
\bibitem{Fuzzy} Wang, Yongqiao, Shouyang Wang, and K.K. Lai. "A New Fuzzy Support Vector Machine to Evaluate Credit Risk." IEEE Transactions on Fuzzy Systems 13, no. 6 (December 2005): 820?31. doi:10.1109/TFUZZ.2005.859320.
\bibitem{Lessmann} Lessmann, Stefan, Hsin-Vonn Seow, Bart Baesens, and Lyn Thomas. "Benchmarking State-of-the-Art Classification Algorithms for Credit Scoring: A Ten-Year Update," n.d.
\bibitem{Brown} Brown, Iain, and Christophe Mues. "An Experimental Comparison of Classification Algorithms for Imbalanced Credit Scoring Data Sets." Expert Systems with Applications 39, no. 3 (2012): 3446-53.
\bibitem{Baesens} Baesens, B., T. Van Gestel, S. Viaene, M. Stepanova, J. Suykens, and J. Vanthienen. "Benchmarking State-of-the-Art Classification Algorithms for Credit Scoring." The Journal of the Operational Research Society 54, no. 6 (June 1, 2003): 627-35.
\bibitem{Neuro-Fuzzy} Pal, S. K., R. K. De, and J. Basak. "Unsupervised Feature Evaluation: A Neuro-Fuzzy Approach." IEEE Transactions on Neural Networks / a Publication of the IEEE Neural Networks Council 11, no. 2 (2000): 366-76. doi:10.1109/72.839007.
\bibitem{SVN_Neural}Huang, Zan, Hsinchun Chen, Chia-Jung Hsu, Wun-Hwa Chen, and Soushan Wu. "Credit Rating Analysis with Support Vector Machines and Neural Networks: A Market Comparative Study." Decision Support Systems, Data mining for financial decision making, 37, no. 4 (September 2004): 543-58. doi:10.1016/S0167-9236(03)00086-1.
\bibitem{RandomForestImbalanced} Chen, Chao, Andy Liaw, and Leo Breiman. Using Random Forest to Learn Imbalanced Data. University of California at Berkeley, Berkeley, California: Statistics Department, University of California, Berkeley, 2004. Web. Statistics Technical Reports.
\bibitem{Saranya} Saranya, R., and C. Yamini. "Survey on Ensemble Alpha Tree for Imbalance Classification Problem." n. pag. Google Scholar. Web. 10 Nov. 2014.
\bibitem{Powell} Warren B. Powell, Ilya O. Ryzhov. "Optimal Learning".
\bibitem{Kaggle} https://www.kaggle.com/c/loan-default-prediction/data
\bibitem{Friedman1} Friedman, Jerome H. "Greedy Function Approximation: A Gradient Boosting Machine." The Annals of Statistics 29, no. 5 (October 1, 2001): 1189?1232.
\bibitem{Friedman2} Friedman, Jerome H. "Stochastic Gradient Boosting." Computational Statistics \& Data Analysis, Nonlinear Methods and Data Mining, 38, no. 4 (February 28, 2002): 367?78. doi:10.1016/S0167-9473(01)00065-2.
\bibitem{sklearn-gbc} http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.GradientBoostingClassifier.html
\bibitem{ElementsStatisticalLearning} Hastie, Trevor. The Elements of Statistical Learning: Data Mining, Inference, and Prediction. 2nd ed. Springer Series in Statistics. New York, NY: Springer, 2009.
\bibitem{Kanamori} Kanamori, Takafumi, Akiko Takeda, and Taiji Suzuki. ?Conjugate Relation between Loss Functions and Uncertainty Sets in Classification Problems.? Journal of Machine Learning Research 14 (2013): 1461?1504.
\bibitem{Masnadi} Masnadi-shirazi, Hamed, and Nuno Vasconcelos. ?On the Design of Loss Functions for Classification: Theory, Robustness to Outliers, and SavageBoost,? 1049?56, 2008. 
\bibitem{ColumbiaRisk} $\mbox{http://www.columbia.edu/~gi10/Talks/EC2012.pdf}$
\bibitem{Rockafellar} Rockafellar, R. Tyrrell, and Stanislav Uryasev. "Conditional Value-at-Risk for General Loss Distributions." Journal of Banking and Finance, 2002, 1443?71.
\bibitem{Takeda} Takeda, Akiko, and Masashi Sugiyama. "?-Support Vector Machine as Conditional Value-at-Risk Minimization." In In Proceedings of the 25 Th International Conference on Machine Learning, 1056?63, 2008.
\bibitem{Tsyurmasto}Tsyurmasto, Peter, Michael Zabarankin, and Stan Uryasev. "Value-at-Risk Support Vector Machine: Stability to Outliers." Journal of Combinatorial Optimization 28, no. 1 (December 5, 2013): 218?32. doi:10.1007/s10878-013-9678-9.
\bibitem{Kashima1}Kashima, Hisashi. Risk-Sensitive Learning via Expected Shortfall Minimization, n.d.
\bibitem{Kashima2}Kashima, Hisashi. ?Risk-Sensitive Learning via Minimization of Empirical Conditional Value-at-Risk.? IEICE Transactions 90-D (2007): 2043?52. doi:10.1093/ietisy/e90-d.12.2043.


\end{thebibliography}


\end{document}