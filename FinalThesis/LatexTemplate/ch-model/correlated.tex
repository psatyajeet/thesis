\section{Modeling Correlated Loan Outcomes} 
\label{sec:model:correlated}

Thus far, we have assumed that the loan outcomes in our dataset are uncorrelated. In the following section, we approach the loan classification problem with the assumption that loan outcomes are correlated. In order to evaluate the different classification methods discussed thus far against correlated loan outcomes, we need to create a synthetic dataset. 

\subsection{Background}

We know that for the German Loan Dataset, the response variable is a binary variable, either a $0$ corresponding to no default or a $1$ corresponding to a default. 

We remember from our literature review in Section ~\ref{sec:litreview:risk-sensitive} that we chose a risk measure to map the random loss from a decision to the real numbers in a way that satisfied a set of axioms. One of these axioms was that diversification of a portfolio of accepted loan applications reduces the risk realized in the portfolio. This axiom is satisfied by the CVaR because it is a convex risk measure and because we assume that the realizations from each decision are identically and independently distributed. 

According to the central value theorem, if these results are I.I.D., then the expected value varies by a factor of $n$ with a larger portfolio while the expected risk metric varies by a factor of $\sqrt{n}$. This is why it is ideal to diversify the portfolio by taking on more I.I.D distributed loans in order to reduce the risk. 

However, this assumption becomes invalid when the response variables are not independent, but rather slightly correlated. In this situation, diversification by having a larger loan portfolio does not reduce the risk. 

In order to model correlated outcomes for loan applications, we assume there is a state of the world from which the loan outcomes are sampled. This state of the world can be approximately thought of as the state of the economy from which the loan outcomes are sampled. Take a scenario where the economy is in a positive state. If the loan outcomes are correlated to this state of the world, then they are more likely to be non-defaults. If the economy is in a poor state (one where people do not have much money to pay back loans), then individual outcomes are more likely to be defaults. 

In our situation, the state of the world will either be chosen or be sampled from a distribution. However, one can imagine using economic factors to create a ``public economic index" that can be used as the state of the world. 

\subsection{Model}

Our model for correlated loan outcomes has three parameters. The state of the world, $W$ is a Gaussian random variable with mean $0$ and standard deviation $1$. Then, we create another Gaussian random variable, $Z$ with mean $\mu$ and standard deviation $\sigma$ (that can be set) that is correlated with the state of the world. The correlation, $\rho$, dictates how closely the individual loan outcomes are correlated to the state of the world. We indirectly sample loan outcomes by generating a Gaussian distribution that takes into account both the state of the world and the correlation and then assigning an outcome of $1$ (loan default) if the variable is greater than $0$ and an outcome of $0$ (no default) if the variable is less than $0$. This is essentially a Bernoulli random variable generated from the Gaussian. 

\begin{figure}[!h]
	\centering
	\begin{tikzpicture}
	
	\def \n {6}
	\def \radius {3cm}
	\def \margin {8} % margin in angles, depends on the radius
	\tikzstyle{main}=[circle, minimum size = 10mm, thick, draw = black!80, node distance = 16mm]
	\tikzstyle{connect}=[-latex, thick]
	
	\node[main] (W) [label=below:$W$] {};
	\foreach \s in {1,...,\n}
	{
		\node[draw, circle, fill = black!10, minimum size = 10mm, label=below:$y_\s$] (y) at ({360/\n * (\s - 1)}:\radius) {};
		\path (W) edge [connect] node [below] {$\rho$} (y);
	}
	\end{tikzpicture}
	\caption{Sampling Correlated Default Outcomes}
\end{figure}

Here, we describe the generative process for each loan outcome: 

\begin{enumerate}
	\item First, determine the state of the world $W$. This is a Gaussian variable with mean $0$ and standard deviation $1$. 
	\item Choose the correlation $\rho$ of the state of the world with each loan outcome. 
	\item Sample $N$ loan outcomes from $Y = \rho W + \sqrt{1-\rho^2}Z$. Each outcome is a Gaussian distribution is correlated with the state of the world, $W$. Here, $Z$ is a second Gaussian distribution with mean $\mu$ and standard deviation $\sigma$. From this $Y$, determine the outcome as being in state 1 (default) if $Y_i \geq 0$ and state 0 (no-default) if $Y_i < 0$.  
\end{enumerate}
\newpage
\begin{enumerate}
	\item First, determine the state of the world $W$. This is a Gaussian random variable.
	\item Choose the correlation $\rho$ of the state of the world with each loan outcome. 
	\item Sample $N$ loan outcomes from $Y = \rho W + \sqrt{1-\rho^2}Z$. Each outcome is a Gaussian distribution, correlated with the state of the world, $W$. Here, $Z$ is a second Gaussian distribution. From this $Y$, determine the outcome as being in state 1 (default) if $Y_i \geq 0$ and state 0 (no-default) if $Y_i < 0$.  
\end{enumerate}

\vspace{5mm}
% \vfill

\begin{figure}[!h]
	\centering
	\begin{tikzpicture}
	\tikzstyle{main}=[circle, minimum size = 10mm, thick, draw =black!80, node distance = 16mm]
	\tikzstyle{connect}=[-latex, thick]
	\tikzstyle{box}=[rectangle, draw=black!100]
	
	\node[main] (W) [label=below:$W$] {};
	\node[main, fill = black!10] (y) [right=of W,label=below:$y_n$] {};
	
	\path (W) edge [connect] node [below left] {$\rho$} (y) {};
	\node[rectangle, inner sep=0mm, fit= (y),label=below right:N, xshift=0mm] {};
	\node[rectangle, inner sep=9mm, draw=black!100, fit = (y)] {};
	\end{tikzpicture}
	\caption{Graphical Model Notation for Correlated Default Outcomes}
\end{figure}

We can show that our formulation satisfies the requirements for correlation between the state of the world $W$ and the loan outcomes $Y$.

Let's say $F_X$ is a Gaussian distribution. 
\[ \text{We generate a }W \sim F_X \text{, and } Z \sim F_X \text{(independent of } W \text{)} \]
\begin{equation}
\text{Let } Y = \rho W + \sqrt{1-\rho^2}Z
\end{equation}
\myequations{Modeling Correlated Loan Outcomes}
\[ cov(W, Y) = cov(W, \rho W) + cov(W, \sqrt{1-\rho^2}Z)\]
\[ cov(W, Y) = cov(W, \rho W) + 0\]
\begin{equation}
cov(W, Y) = \rho \cdot var(W)
\end{equation}
We know that $Var(Y) = Var(W)$ because $Z$ has the same distribution as $W$
\[ cor(W, Y) = \frac{cov(W, Y)}{\sqrt{var(W)^2}} = \frac{\rho \cdot var(W)}{var(W)}  \]
\begin{equation}
cor(W, Y)  = \rho
\end{equation}
\myequations{ Correlation of State of World to Loan Outcomes}

Using this method and by varying the parameters, we generate different datasets with correlated loan outcomes upon which to apply and evaluate our previously discussed risk-sensitive classification methods.  





