\section{Risk-Sensitive Classification} 
\label{sec:model:risk-sensitive}

In the previous section, we discussed a model for risk-neutral classification. This was a formulation of the problem that is useful for many cases where the objective is to minimize the probability of misclassification. However, ours is not an application where we simply want to  minimize the probability of misclassification. The cost of a mistake varies for different decisions and moreover, the magnitude of this cost can vary in different cases. 

This leads us to consider cost-sensitive classification. The primary benefit of using cost-sensitive learning is that it minimizes the expected cost of misclassification rather than the probability of misclassification. This is suitable in the case that costs are variable throughout the data. 

Before we begin, it is worth describing all the variables that will be used in this section:

\singlespacing
\begin{align*}
x &= \text{target objects, or feature set}\\
y &= \text{class labels}\\
\theta &= \text{model parameters} \\
c &= \text{cost, a random variable that indicates how bad an action} y \text{taken on } x \text{ is} \\
h &= \text{hypothesis, a function used to predict the class label of a target, }x\\
D &= \text{data distribution} \\
E &= \text{training examples, sampled from } D \\
i &= \text{single example, or loan application, in training set} \\
N &= \text{size of training set} \\
\beta &= \text{quantile of cost distribution of interest}, 0 \leq \beta \leq 1 \\
\alpha &= \text{Value-at-Risk} \\
\phi &= \text{Conditional Value-at-Risk}\\
\end{align*}
\doublespacing

We use logistic regression for our classification. Logistic regression estimates the posterior probability of the positive class evaluated from:

\[ \hat{p_i} = P(y=1|x_i) = h_i(\theta, x_i)= g\Big( \sum_{j=1}^k \theta^j x^j_k\Big) \]

where $g(\cdot)$ is the logistic sigmoid function, which is defined as $g(z) = 1/(1+e^{-z})$. 

In logistic regression, we must find the right parameters to minimize a given cost function

\[ J(\theta) = \frac{1}{N} \sum_{i=1}^N J_i(\theta) \]

In most cases of logistic regression, the loss function is the negative logarithm of the likelihood:

\[ J_i(\theta) = -y_i \log{(h_i(\theta, x_i))} - (1-y)\log{(1-h_i(\theta, x_i))} \]

However, remember that this loss function assigns the same weight to all misclassifications. Instead, we want our loss function to incorporate cost. Thus, the example-dependent cost function we'll use is as follows (note that $h_i(\theta)$ is the prediction for training example $i$ based on $x_i$):

\begin{align*} 
\label{cost_function}
Cost(F(\theta)) = \sum_{i=1}^N \Big(y_i(h_i(\theta, x_i)C_{TP_i} + (1-h_i(\theta, x_i))C_{FN_i}) \\ + (1-y_i)(h_i(\theta, x_i)C_{FP_i} + (1-h_i(\theta, x_i))C_{TN_i}) \Big)
\end{align*}
\myequations{Cost Function for Example-Dependent Cost-Sensitive Classifier}

Still, for the loan default classification problem, this is insufficient because a cost-sensitive approach does not ``aggressively suppress the occurrence of huge costs" ~\cite{kashima_risk-sensitive}. 

\subsection{Defining Conditional Value-at-Risk}
\label{subsec:model:risk-sensitive:cvar}

Just as portfolio theory tries to maximize profits while avoiding the risk of huge losses that have low probability, so we must approach loan default classification. We must find an appropriate risk metric to use to gauge the risk of incurring huge costs. Here, we use the conditional value-at-risk, or expected shortfall. 

Our classification algorithm will minimize this conditional value-at-risk as the objective function. Let us define conditional value-at-risk. In the loan default prediction model, we define the conditional value-at-risk $\phi_\beta^D(\theta)$ with respect to the hypothesis $h$ and data distribution $D$ ~\cite{kashima_risk-sensitive} as:

\[ \phi_{\beta}^D(\theta) = \frac{1}{1-\beta}   E_D [I(c(x, h(\theta)) \geq \alpha^D_\beta(\theta))c(x, h(\theta))]\]

This value indicates the expected cost above a certain value-at-risk, or the ``expectation of the top $100(1-\beta)\%$ costs given a constant $0 \leq \beta \leq 1$. 

Here, $\alpha^D_\beta(\theta)$, the value-at-risk (VaR) is defined as:

\[ \alpha^D_\beta(\theta) = \min \Big\{ \alpha \in  \mathbb{R} |  E_D[I(c(x, h(\theta)) \geq \alpha)] \leq 1-\beta \Big\} \] 

where $I$ is an indicator function that evaluates to 1 when the argument is true and 0 when the argument is false. The argument for the indicator function is whether the cost of a given prediction (hypothesis $h$) for a given loan applicant is greater than the value-at-risk, $\alpha$. We are finding the expected value of this indicator function which yields the proportion of prediction costs that are greater than the value-at-risk. We want to set our $\alpha$ to the minimum value such that this probability is less $1-\beta$. 

In Figure ~\ref{expected-shortfall}, you can see a graphical interpretation of the conditional value-at-risk.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{ch-model/figures/expected_shortfall}
	\caption{Conditional Value-at-Risk (Expected Shortfall) \cite{kashima_risk-sensitive}}
	\label{expected-shortfall}
\end{figure}

We can rewrite $\phi_{\beta}^D(\theta)$ because the conditional value-at-risk is just the expected cost of misclassification that surpasses the value-at-risk:

\begin{equation} \label{cvar_general}
\phi_{\beta}^D(\theta) = \alpha^D_\beta(\theta) + \frac{1}{1-\beta}  E_D {[c(x, h(\theta))-  \alpha^D_\beta(\theta)]}^+
\end{equation} 

where the function ${[x]}^+$ returns $x$ if positive or $0$ if negative. 

\subsection{Defining a Risk-Sensitive Learner to Minimize Conditional Value-at-Risk}
\label{subsec:model:risk-sensitive:learner}

We want to use an algorithm that optimizes $\theta$, the model parameters for our risk-sensitive classification model. 

Because we do not know the true distribution of our data, $D$, we alter the model to use the training examples $E$ instead of $D$.

\begin{equation} \label{cvar_data}
\phi_{\beta}^E(\theta) = \alpha^E_\beta(\theta) + \frac{1}{(1-\beta)N} \sum_{i=1}^N {[c(x, h(\theta))-  \alpha^E_\beta(\theta)]}^+ 
\end{equation}
\myequations{Our formulation of Conditional Value-at-Risk}

\begin{equation} \label{var_data}
\alpha^E_\beta(\theta) = \min \Big\{ \alpha \in  \mathbb{R} |  \frac{1}{N} \sum_{i=1}^N [I(c(x, h(\theta)) \geq \alpha)] \leq 1-\beta \Big\}
\end{equation} 
\myequations{Our formulation of Value-at-Risk}

We can assume that the value at risk $\alpha^E_\beta(\theta)$, for a given $E$ and $\beta$, is a known constant that we will call $\bar{\alpha}$ so we only have to minimize the second term in \eqref{cvar_data}. The term we want to minimize then is the following:

\begin{equation} 
\label{minimize_c}
\bar{C}^E_{\bar{\alpha}} := \frac{1}{N} \sum_{i=1}^N {[c(x^{(i)}, h(\theta)) - \bar{\alpha}]}^+
\end{equation}
\myequations{Optimization function for classifier in MetaRisk}

We have algorithms to find model parameters, $\theta$, that minimize \eqref{minimize_c}. In our case, we will use cost-sensitive logistic regression, but we could have used many other learners. After finding this local minimized CVaR for the given $\bar{\alpha}$, we fix $\theta$, and find the new VaR \eqref{var_data} for that $\theta$: 

\begin{equation} 
\label{calculate_var}
\alpha^E_\beta(\theta) = \min_{k \in N} \Big\{ c(x_k, h(\theta)) |  \frac{1}{N} \sum_{i=1}^N [I(c(x_i, h(\theta)) \geq I(c(x_k, h(\theta)))] \leq 1-\beta \Big\}
\end{equation} 
\myequations{Updating formula for Value-at-Risk}

This is the same as saying the new VaR is the $c(x_k, h(\theta))$ with $k$ being the index of the training data point with the $((1-\beta)N)^{\text{th}}$ largest cost (for model parameters $\theta$). We can solve for the new VaR by sorting the costs (for model parameters $\theta$) and choosing k-th training data point as our new $\alpha$. 

Using this understanding, Kashima ~\cite{kashima_risk-sensitive} developed a risk-sensitive meta-learning algorithm called MetaRisk. The algorithm minimizes the conditional value-at-risk using already existing cost-sensitive learners. This MetaRisk algorithm is summarized in Algorithm ~\ref{MetaRisk}. 

\begin{algorithm}[H]
	[Step 1:] Set $\bar{\alpha} := 0$\;
	[Step 2:] For $\bar{\alpha}$, find $\theta' = \arg \min_\theta \bar{C}^E_{\bar{\alpha}}(\theta)$, and set $\theta = \theta'$\;
	[Step 3:] For this new $\theta$, find the empirical VaR $\alpha^E_\beta(\theta)$ and let $\bar{\alpha} := \alpha^E_\beta(\theta)$\;
	[Step 4:] Continue steps 2 and 3 until we converge on the estimator of the conditional value-at-risk: $F^E_\beta(\theta, \bar{\alpha})$\;
	\caption{MetaRisk algorithm}
	\label{MetaRisk}
\end{algorithm}

We can ``recycle" existing cost-sensitive learners and make them risk-sensitive by using reweighted costs in each iteration of the MetaRisk algorithm. The cost in each iteration is the amount by which the value-at-risk is exceeded for each alternative loan. 