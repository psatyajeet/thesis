\section{Risk-Sensitive Classification} 
\label{sec:results:risk-sensitive}

Here, we applied the model of risk-sensitive classification (formulated in section ~\ref{sec:model:risk-sensitive}) to the German credit data set. Our goal is to predict whether a particular customer will default on his loan based on his application. In our situation, we classify a default as a ``positive" event (assigned a 1) and non-default as a ``negative" event (assigned a 0). Each dataset has a different profile for the applicants. There are two different misclassification costs in each case:

\begin{enumerate}
	\item Classifying a ``good" applicant as a ``bad" applicant loses potential profit from interest on the loan. We call this a false positive (predict 1 where true classification is 0). 
	\item Classifying a ``bad" applicant as a ``good" applicant loses a portion of the loan (a default). We call this a false negative (predict 0 where true classification is 1).
\end{enumerate}


\subsection{German Credit Data Set}

This dataset from the STATLOG PROJECT ~\cite{_uci_????} contains information about 1000 customers, 700 good applicants and 300 bad applicants. Using our model notation, $x$ is the features that make up each profile and here they are 20 attributes including sex, age, job, credit history, loan purpose, and property ownership status. There is a slightly modified version of the dataset that contains 24 attributes in a numerical (quantitative) format ~\cite{_uci_????}. 

In order to define example-dependent costs for this dataset, we used a procedure similar to Kashima ~\cite{kashima_risk-sensitive_2007} to assign misclassification costs. True positive and true negative classifications incurred zero costs. 

A false positive results in a loss of profit from interest paid by the applicant on their loan because we chose not to grant a loan due to our prediction. The misclassification cost for this case was $0.1 \cdot \frac{duration}{12} \cdot amount$, where 10\% was the assumed interest rate.  

A false negative results in a default of the loan, where the applicant  pay their loan amount. The misclassification cost for this case was $0.30 \cdot amount$, where we set the default amount to be 30\% for each loan. This is an assumption that could be played with. Perhaps, we could have even randomly sampled default amounts in future work. Figure ~\ref{german-cost-matrix} illustrates these costs. 

\begin{figure}[h]
	\centering
	\begin{tabular}{|c|c|c|}
		\hline
		& \begin{tabular}[c]{@{}c@{}}Actual Positive\\ $y_i = 1$\end{tabular} & \begin{tabular}[c]{@{}c@{}}Actual Negative\\ $y_i = 0$\end{tabular} \\ \hline
		\begin{tabular}[c]{@{}c@{}}Predicted Positive\\ $h_i = 1$\end{tabular} & $C_{TP_i} = 0$                                                           & $C_{FP_i} = 0.1 \cdot \frac{duration}{12} \cdot amount$                                                           \\ \hline
		\begin{tabular}[c]{@{}c@{}}Predicted Negative\\ $h_i = 0$\end{tabular} & $C_{FN_i} = 0.30 * amount$                                                           & $C_{TN_i} = 0$ \\ \hline                                                 
	\end{tabular}
	\caption{German Credit Data Cost Matrix}
	\label{german-cost-matrix}
\end{figure}


Now, we take look at Figures ~\ref{lr-histogram}, ~\ref{cs-histogram}, ~\ref{rs-histogram} which show the cost distributions resulting from risk-neutral logistic regression, cost-sensitive logistic regression, and risk-sensitive logistic regression trained and tested at $\beta=0.95$ respectively. 

\begin{figure}[H]
	\centering
	\includegraphics[width=0.75\textwidth]{ch-experiments/figures/lr_histogram1}
	\caption{Cost distribution on logistic regression of German Credit Data}
	\label{lr-histogram}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.75\textwidth]{ch-experiments/figures/cost_sensitive_histogram1}
	\caption{Cost distribution for CSLR of German Credit Data}
	\label{cs-histogram}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.75\textwidth]{ch-experiments/figures/risk_histogram1}
	\caption{Cost distribution at $\beta = 0.95$ for the German Credit Data}
	\label{rs-histogram}
\end{figure}

The first clear observation from these histograms is that the cost distributions are not Gaussian. Instead, the costs are significantly right, or positive, skewed. There is a heavy long right tail reflected in the cost distribution from all three classifiers. We see that all three cost distributions have many outcomes with zero cost (because they were classified either as True Positives or True Negatives). Then, there is a steep tail to the right that is somewhat fat-tailed, especially for the logistic regression (LR) and the cost-sensitive logistic regression (CSLR). For example, in the LR and CSLR cost distributions, we see a small increase in costs between 20 and 25. We see another, smaller, spike in costs between 42 and 48. When we compare this to cost distribution for risk-sensitive logistic regression (RSLR), we see less of an spike at any point in the tail.  There is still a small spike in the histogram for costs between 42 and 48 (as observed in the other two plots).

\begin{figure}[H]
	\centering
	\includegraphics[width=0.75\textwidth]{ch-experiments/figures/all_three}
	\caption{Cost distribution of all three classifiers}
	\label{all-three}
\end{figure}

Figure ~\ref{all-three} shows the cost distributions for all three learners superimposed on each other. Here, we can more clearly see the small spikes in the cost distribution compared to each other. As discussed earlier, the CSLR and RSLR have a higher portion of costs at 0 than the LR whereas the LR demonstrates more of the fat, right tail distribution. 

The takeaway from these plots is that the costs do indeed demonstrate long, fat tail properties which give us reason to consider risk-sensitive learning. Even the mild improvement in costs at the end of the right tail in the RSLR compared to the LR and CSLR shows the value in our risk-sensitive methods. 

\subsubsection{Results}

\begin{table}[H]
	\footnotesize
	\caption{Risk-Sensitive Logistic Regression on German Data Set}
	\begin{tabular}{cl|lllll}
		\multicolumn{1}{l}{} & \multicolumn{1}{c|}{} & \multicolumn{5}{c}{Test $\beta$}                                                                                                               \\
		\multicolumn{1}{l}{} & \multicolumn{1}{c|}{} & \multicolumn{1}{l}{$\beta=0.80$} & \multicolumn{1}{l}{$\beta=0.90$} & \multicolumn{1}{l}{$\beta=0.95$} & \multicolumn{1}{l|}{$\beta=0.99$}        & Cost \\ \cline{2-7} 
		    & Cost-Neutral          & 15.83 (5.72)               & 22.75 (12.85)              & 28.65 (18.05)              & \multicolumn{1}{l|}{39.57 (31.87)} & 389.72    \\
		& Cost-Sensitive        & 14.96 (5.17)               & 22.10 (11.39)              & 29.04 (19.10)              & \multicolumn{1}{l|}{39.33 (33.33)} & \textbf{346.13}     \\
		\multirow{4}{*}{\rotatebox{90}{Training $\beta$}}& $\beta=0.80$                & 15.56 (5.33)               & 23.44 (11.91)              & 31.48 (19.78)              & \multicolumn{1}{l|}{47.67 (33.27)} & 388.62     \\
		& $\beta=0.90$             & 15.38 (5.37)               & 22.32 (11.77)              & 29.41 (18.77)              & \multicolumn{1}{l|}{41.82 (32.12)} & 378.07     \\
		& $\beta=0.95$                & \textbf{14.72 (5.10)}               & \textbf{21.40 (11.52)}              & \textbf{28.25 (18.21)}              & \multicolumn{1}{l|}{42.02 (30.92)} & 367.59     \\
		& $\beta=0.99$                & 14.74 (5.30)               & 21.52 (11.74)              & 28.44 (18.44)              & \multicolumn{1}{l|}{\textbf{39.87 (30.67)}} & 371.99    
	\end{tabular}
	\label{rs-results}
\end{table}

Table ~\ref{rs-results} shows the results of risk-sensitive logistic regression on the German Credit Data Set. The measurements were taken from the average values of ten-fold stratified cross validation (technique discussed in Section ~\ref{sec:litreview:cross-validation}).

The row labeled Cost-Neutral indicates the results of risk-neutral logistic regression. The row labeled Cost-Sensitive indicates the results using the cost-sensitive logistic regression. The rows labeled Training $\beta$ indicate the results by the MetaRisk algorithm with training $\beta = 0.80, 0.90, 0.95, \text{and } 0.99$. The columns show the resulting values of the CVaR (and VaR in parentheses) for each of the test $\beta$. The last column indicates the total cost of classification for each of the learners.

When we take a look at these results, a few observations jump out immediately. 

First, we compare the total misclassification cost incurred by each of the classification models we tried. We see here that, as expected, the cost-sensitive classifier had the lowest total cost of all of the classifiers. The cost-sensitive classifier was by far the best at minimizing the total misclassification cost. The risk-sensitive classifiers performed worse than the cost-sensitive classifier but better than the cost-neutral classifier at minimizing total misclassification cost. This makes some sense because these conditional value-at-risk based classifiers take cost into some account, indirectly through minimizing the risk measure, but less so than the pure cost-sensitive classifier. The cost-neutral classifier has the highest total cost because it does not account for costs whatsoever. 

Now, we compare the conditional value-at-risk, our risk measure, incurred by each of the classification models we tried. We see that the MetaRisk learners achieve lower CVaR than both the cost-neutral and cost-sensitive learners. We see that the MetaRisk trained for $\beta=0.95$ performed well across all the test $\beta$ except for at the $\beta=0.99$ level. This is surprising to us because we would expect the MetaRisk trained for a particular training $\beta$ to perform better for the corresponding test $\beta$. This is something to explore with different types of cost-sensitive learners (other than the one we used).