\section{Classification}
\label{sec:overview:classification}

A binary classifier, like the one we'll be using, identifies whether a loan is likely to be a default or non-default. The first step is to calculate a probability of default for a loan application. Using a particular threshold for the probability, we can classify each loan application as likely to result in a default or unlikely to result in a default. By establishing this classification process, a loan provider can train a model and use it to take any new loan application and probabilistically determine whether or not the loan will default. 
 
\subsection{Risk-Neutral}

At its core, the classification problem is a decision problem. More specifically, it decides the class to assign to a loan classification (either default or non-default) whose appropriate class is unknown. A risk neutral binary classifier tries to minimize the error or loss between the classifier prediction and the truth. This classifier does not take risk into account.  

\subsection{Risk-Sensitive}

\subsubsection{Drawbacks of Risk-Neutral Classification}
From the previous section, we see the loan default classification is a decision problem in which we decide whether or not to grant a loan. This decision problem will result in a random loss, or cost, dependent on the loan application, decision, and true outcome. A risk-neutral classifier does not take this outcome cost into account. Instead, it typically aims to minimize the probability of misclassification, treating each mistake equally. Essentially, the cost of a mistake is 1 whereas the cost of a correct classification has is 0. 

\subsubsection{Drawbacks of Cost-Sensitive Classification}

A natural next step to such a decision problem, where the actual cost of misclassification is important, is to optimize (minimize) the expected value of misclassification cost instead of the misclassification probability. This works in most situations where all costs can be treated equally or close to equally.

\subsubsection{Risk-Sensitive Classification}
However, for the credit evaluation problem there is a possibility of accepting high-risk loans when using expected value optimization. This can lead to credit institutions being bankrupted ~\cite{iyengar_optimization} if these high-risk loans default. We see that for the loan default prediction problem, it is valuable to focus more on minimizing disastrous, high-risk defaults. For the loan default classification problem, it makes sense to use a new objective function that strongly avoids the risk of huge costs (tail risk). 

\subsection{Correlated Loan Outcomes}
Thus far, we have approached the classification of each client as an independent and isolated problem. We assume that the outcome of each client (amount of default) is independent and identically distributed. However, this is not an entirely accurate picture of how loans function in the real world. 

As we saw with the subprime mortgage crisis and general analysis of loans, we know that loans can be correlated with each other. That is, if one loan defaults, it is likely that other loans will also default because of the complex correlations between loans within the market environments. 

The mechanisms by which this happens are complex and not in the scope of this thesis. However, it is valuable to see how our models perform when dealing with loan applicants with correlated outcomes. 

