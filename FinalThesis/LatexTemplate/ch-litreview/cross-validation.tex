\section{Cross Validation} 
\label{sec:litreview:cross-validation}


When learning the parameters of a classification model, it is typical to discover the parameters using a training set and then determine the effectiveness of these parameters using a seperate test set. It is a mistake to use the same training and test set for both tasks, because this results in potential overfitting. We hold out data to use as a test set in order to avoid overfitting. We can see in figure \ref{validation} what this looks like. The observations are randomly split into the training set (in blue) and the test set (in orange). The learner is fit on the training set and the performance of the learner is evaluated on the test set.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\textwidth]{ch-litreview/figures/validation}
	\caption{Validation Set \cite{james_introduction_2013}}
	\label{validation}
\end{figure}


While this method of using a validation set is valuable for testing the effectiveness of the learner, it still has some flaws. For one, the estimate of the test error can be variable and dependent on which exact observations were chosen for the training set. Second, since the training set used to determine the model parameters is smaller than the whole data set, the error rate may overestimate the overall error rate when the learner is fit on the entire data set. This is because most statistical learners tend to perform worse on smaller data sets. 

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\textwidth]{ch-litreview/figures/k-fold}
	\caption{$k$-Fold Cross Validation \cite{james_introduction_2013}}
	\label{k-fold}
\end{figure}

This is where cross validation comes in. The most basic version of cross validation (CV) is $k$-fold CV. Here, the observations are split into $k$ smaller groups, where each of these groups is called a ``fold". The first fold is used as the test set and the remaining $k-1$ folds are used as the training set. This procedure is repeated $k$ times, where a different fold is held out as the test set. The $k$-fold CV estimate is taken as the mean of the test statistic. Figure ~\ref{k-fold} helps illustrate the process of $k$-fold cross validation. Again, the observations are split into $k$ folds and in each trial, one fold (in orange) is held out as the test set where the remaining folds (in blue) are used as the training set. 


Doing $k$-fold CV can be computationally expensive, requiring careful selection of $k$, but overall it provides value in finding a more accurate error rate. However, the $k$-fold CV is still not ideal for our problem because the folds may not be representative of the percentage of samples from each class in the overall data set.  Since our loan dataset is imbalanced, we used a specialized version of CV known as Stratified $k$-Fold Cross Validation. This version of $k$-fold cross validation creates folds that preserve the percentage of samples from each class. 




