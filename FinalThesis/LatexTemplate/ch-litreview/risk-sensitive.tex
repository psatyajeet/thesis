\section{Risk-Sensitive Optimization for Classification} 
\label{sec:litreview:risk-sensitive}

Literature on risk-averse optimization functions generally focuses on portfolio optimization and similar applications. For our purposes, we are looking for an objective function formulation that incorporates risk so that we can train a classifier that minimizes this risk.

\iffalse
\begin{figure}[H]
	\centering
	\label{classification-cost-matrix}
	\includegraphics[]{ch-litreview/figures/classification_cost_matrix}
	
	\caption{Classification Cost Matrix \cite{bahnsen_example-dependent_2014}}
\end{figure}
\fi

\begin{figure}[h]
	\centering
		\begin{tabular}{c|c|c}
			& \begin{tabular}[c]{@{}c@{}}Actual Positive\\ $y_i = 1$\end{tabular} & \begin{tabular}[c]{@{}c@{}}Actual Negative\\ $y_i = 0$\end{tabular} \\ \hline
			\begin{tabular}[c]{@{}c@{}}Predicted Positive\\ $h_i = 1$\end{tabular} & $C_{TP_i}$                                                           & $C_{FP_i}$                                                           \\ \hline
			\begin{tabular}[c]{@{}c@{}}Predicted Negative\\ $h_i = 0$\end{tabular} & $C_{FN_i}$                                                           & $C_{TN_i}$                                                                  
		\end{tabular}
	\caption{Classification Cost Matrix \cite{bahnsen_example-dependent_2014}}
	\label{classification-cost-matrix}
\end{figure}

We must remind ourselves that in risk-sensitive classification, we don't want to minimize the probability of misclassification, but rather minimize a risk measure based on the cost of misclassification. In Figure ~\ref{classification-cost-matrix}, we see the four possible outcomes for a classification and the costs associated with them. In the loan default model, a loan default is a ``positive" result ($y_i=1$) and no-default is a ``negative" result ($y_i=0$). While this nomenclature is slightly confusing, it is consistent with our dataset and also with the idea that no-default corresponds to $0\%$ default. The possible costs are associated with True Positive (TP), False Positive (FP), False Negative (FN), and True Negative (TN). 

Here, we encounter the idea of example-dependent cost-sensitivity. This is a very simple concept, that the cost-sensitivity of an outcome, whether TP, FP, FN, or TN is dependent on the specific example in the dataset. Because each example has its own loan amount, the cost of a FP or FN varies from example to example. This is a more realistic model than one where the costs are uniform across examples. 

\subsection{Risk Measures}
\label{subsec:risk-measures}

The first task is to find a risk measure that is useful for our application. Iyengar ~\cite{iyengar_optimization} provides a framework for what a risk measure is. The risk measure we will be optimizing is based upon random loss from a decision, where the random loss $\tilde{Z}$ can be defined as:

\[ \tilde{Z} = f(x, \omega), \omega \in \Omega \]

Let us say that there is a space $\mathcal{Z}$ for random variables $\tilde{Z}$. 

A risk measure, $\rho$ is a mapping \[\rho: \mathcal{Z} \rightarrowtail \mathbb{R}\]

Important characteristics of this risk measure will be  ~\cite{iyengar_optimization}:

\begin{enumerate}
	\item Diversification or randomization reduces risk
	\begin{itemize}
		\item for $\tilde{Z_1}, \tilde{Z_2} \in \mathcal{Z}, \alpha \in [0,1] \Rightarrow$\\$\rho(\alpha \tilde{Z_1} + (1-\alpha)\tilde{Z_2}) \leq \alpha\rho(\tilde{Z_1}) + (1-\alpha)\rho(\tilde{Z_2}) $
		\item By definition, this means the risk measure is convex
	\end{itemize}
	\item Higher losses imply higher risk
	\begin{itemize}
		\item if $\tilde{Z_1} \geq \tilde{Z_2}$, \\
		then $\rho(\tilde{Z_1}) \geq \rho(\tilde{Z_2})$
		\item By definition, this means the  risk measure is monotonic
	\end{itemize}
	\item Risk measure is additive over constant increase in losses
	\begin{itemize}
		\item $\rho(\tilde{Z} + a) = a + \rho(\tilde{Z})$
	\end{itemize}
	\item Scaling the random outcome scales the risk
	\begin{itemize}
		\item $\rho(\alpha\tilde{Z}) = \alpha\rho(\tilde{Z})$
	\end{itemize}
\end{enumerate}


\subsection{Conditional Value-at-Risk, or Expected Shortfall}
\label{subsec:litreview:risk-sensitive:cvar}

According to Garud Iyengar of Columbia ~\cite{iyengar_optimization}, some popular constraints on risk are $\beta$-quartile Value-at-Risk (VaR) and Conditional Value-at-Risk (CVaR). The Conditional Value-at-Risk, also commonly referred to as Expected Shortfall, is a spectral risk measure, meaning that is a weighted average of outcomes where bad outcomes are typically included with larger weights. 

We see that the Value-at-Risk and Conditional Value-at-Risk are closely related as defined by Iyengar at the $\alpha$-level quantile:


\begin{align}
\mbox{VaR}_\beta(f(x)) &= \inf \{\alpha : P(f(x)\geq \alpha)) \leq 1-\beta\} \\
\mbox{CVaR}_\beta(f(x)) &= \int_{\beta}^{1} \mbox{VaR}_{\beta}(f(x)) \end{align}
\myequations{Definition of Value-at-Risk and Conditional Value-at-Risk}

According to Rockafeller ~\cite{rockafellar_conditional_2002}, one of the shortfalls of VaR is that it doesn't adequately measure the extent of the losses beyond the value-at-risk. It can't really distinguish the extent of losses that can be incurred past the VaR. The VaR only gives the ``lowest bound for losses in the tail of the loss distribution and has a bias toward optimism instead of the conservatism that ought to prevail in risk management."

The better measure is the conditional value-at-risk. Using the general framework described by Iyengar, we formulate a definition of the conditional value-at-risk (CVaR) optimization as follows:

\begin{align*}
\rho_{CVaR_\beta}(\tilde{Z}) &= \max \mathbb{E}_{\mathbb{P}}[\tilde{\zeta}\tilde{Z}] \\
s.t.  \mathbb{E}_{\mathbb{P}}[\tilde{\zeta}] &= 1,\\  0 \leq \tilde{\zeta} &\leq \frac{1}{1-\beta}
\end{align*}

The conditional value-at-risk, or expected shortfall, is a coherent risk measure. That means it is convex, monotonic, homogeneous, and law-invariant, satisfying all the characteristics of the risk measure we described in section ~\ref{subsec:risk-measures}.

The dual representation of CVaR is more useful and relevant to our classification problem:

\begin{equation}\rho_\beta(\tilde{Z})= \min_{\alpha} \left\{ \alpha + \frac{1}{1-\beta}  \mathbb{E}_P [(\tilde{Z} - \alpha)^{+}]  \right\}
\end{equation}
\myequations{Dual Representation of CVaR}

Kashima explores the use of CVaR for ``risk-sensitive learning" ~\cite{kashima_risk-sensitive} and a model for using it with any classification technique using an algorithm known as MetaRisk. The terminology and formulation used by Kashima is used extensively for my ultimate model of risk-sensitive classification in section ~\ref{sec:model:risk-sensitive}. 

Takeda ~\cite{takeda_-support_2008} and Tsyurmasto ~\cite{tsyurmasto_value-at-risk_2013} provide methods of formulating Support Vector Machine classification in terms of CVaR and VaR minimization. While we won't use this formulation in this thesis, it is a valid alternative (given appropriate changes to incorporate cost-sensitivity ~\cite{fumera_cost-sensitive_2002}).  

There were numerous additional papers that described methods of making different learners cost-sensitive, including SVMs ~\cite{fumera_cost-sensitive_2002}, AdaBoost ~\cite{fan_adacost:_1999}, and decision trees ~\cite{bradford_pruning_1998}. It is worth considering trying these different cost-sensitive classification tools in future work to compare their effectiveness to my methods. 