\section{Risk-Neutral Classification}
\label{sec:litreview:risk-neutral}

\subsection{Logistic Regression and LDA} 

Two basic techniques that were referenced time and again for risk-neutral classification of credit data were Logistic Regression (LR) and Linear Discriminant Analysis (LDA). They are considered to be ``relatively simple and explainable" ~\cite{shi_credit_2013} and are often used as a benchmark for classification models. In fact, those two classification models are considered to be the most commonly used credit scoring techniques. They end up being very competitive when compared to more complex techniques ~\cite{brown_experimental_2012}.

Because we use logistic regression-based classifiers extensively in our model, it is valuable to define logistic regression (described by Hastie \cite{hastie_elements_2009}). 

In its most basic form, the logistic function takes the form 

\[ p(X) = \frac{e^{\beta_0 + \beta_1 X}}{1 + e^{\beta_0 + \beta_1 X}} \]

It is common to use a method known as maximum likelihood to fit data to this model and choose the model parameters $\beta$. Once the model parameters are found, one can find the probability, $p$,  that the training data $x$ falls in the positive class. Using a cutoff for this probability, one can classify each $x$ as being in one of two classes (positive or negative). The formulation is slightly different in cases with more than two classes.  

For our loan default prediction problem, we want to predict a binary response using more than just one predictor. The generalized multiple predictor logistic regression is formulated as follows. 

\[ p(X) = \frac{e^{\beta_0 + \beta_1 X_1 + \cdots + \beta_p X_p}}{1 + e^{\beta_0 + \beta_1 X_1 + \cdots + \beta_p X_p}} \]

For the full mathematical formulation of logistic regression for use as a cost-sensitive classifier, refer to section ~\ref{sec:model:risk-sensitive}. 

\subsection{Support Vector Machines} 

Support Vector Machines are often used for classification and for credit scoring models. For our problem, we are interested in the potential use of SVMs for risk-sensitive classification. To provide more insight and to compare against the logistic regression model we use, we provide a formulation of support vector machines used by Tsyurmasto ~\cite{tsyurmasto_value-at-risk_2013}. The main idea of the support vector machine is to ``map a training data from two classes into a higher or infinite dimensional space and separate the classes with maximal margin" ~\cite{tsyurmasto_value-at-risk_2013}: 

Let $\{(x_1, y_y), \ldots, (x_l, y_l)\}$ be a training set of samples $x_i \in \mathbb{R}^m$ with class labels $y_i \in {-1, +1}$ for all samples $i=1 \ldots l$. These samples are transformed into $\{\phi(x_1), \ldots, \phi(x_l)\} \subset \mathbb{R}^n$ by a mapping $\phi : \mathbb{R}^m \rightarrow \mathbb{R}^n$. The goal of SVM is to find a hyperplane $w^Tx + b = 0, w \in \mathbb{R}^n, b \in \mathbb{R}$ that separates the samples in $\{\phi(x_1), \ldots, \phi(x_l)\}$ into those with class label $y_i=+1$ and those with class label $y_i = -1$. 

Let's say that $\Omega=\{\omega_1, \ldots, \omega_l\}$ is the space of outcomes. We define $x: \Omega \rightarrow \mathbb{R}^n$ and $y: \Omega \rightarrow \{-1, +1\}$ as discrete random variables in a way that $x(\omega_i) = \phi(x_i)$ and $y(\omega_i) = y_i$ for $i = 1, \ldots, l$. For each of the outcomes $\omega \in \Omega$ and decision variables $w \in \mathbb{R}^n$ and $b \in \mathbb{R}$, a loss function is defined as 

\[\mathcal{L}_\omega(w, b) = -y(\omega) \cdot [w^Tx(\omega) + b] \]

The loss is a random variable in space $\{-y_i \cdot [w^Tx(\omega) + b]\}_{i=1}^l$ with equal probability of each outcome being $1/l$.

There are many types of SVMs; two popular ones are hard-margin SVMs that impose the constraint that each training sample is classified correctly and soft-margin SVMs (C-SVM) that have a parameter $C$ that incorporates a trade off between training error and margin size. A third popular SVM, $\nu$-SVM replaces $C$ with a parameter $\nu \in [0,1]$ that specifies the upper bound for the training error. 

The hard-margin SVM is formulated as: 

\[ \min_{w \in \mathbb{R}^n, b \in \mathbb{R}} \frac{1}{2} {||w||}^2 \text{  s.t.  } y_i(w^T\phi(x_i) + b) \geq 1, \text{ for } i=1, \ldots, l  \]

The soft-margin SVM is formulated as:

\[ \min_{w \in \mathbb{R}^n, b \in \mathbb{R}} \Big(\frac{1}{2}{||w||}^2 + C'\sum_{i=1}^l {[-y_i[w^T\phi(x_i) + b] + 1]}_+\Big), \text{ where } C' > 0\]

Finally, the $\nu$-SVM is formulated as:

\[ \min_{w \in \mathbb{R}^n, b \in \mathbb{R}} \Big(\frac{1}{2} {||w||}^2 - \nu \rho + \frac{1}{l}\sum_{i=1}^l {[\rho-y_i(w^T\phi(x_i) + b)}]_+\Big)\]

Tsyurmasto ~\cite{tsyurmasto_value-at-risk_2013} reformulates the $\nu$-SVM in terms of conditional value-at-risk (discussed in Section ~\ref{subsec:litreview:risk-sensitive:cvar}):

\[ \min_{w \in \mathbb{R}^n, b \in \mathbb{R}} \Big(\frac{1}{2} {||w||}^2 + CVaR_{1-\nu}(\mathcal{L}_\omega(w,b)) \Big)\]

or even more simply, 

\[ \min_{w \in \mathbb{R}^n, b \in \mathbb{R}} \frac{1}{2} {||w||}^2 \text{  s.t.  } CVaR_{1-\nu}(\mathcal{L}_\omega(w,b)) \leq -1  \]

This formulation by Tsyurmasto gives us insight into a way of representing SVMs with risk measures. We can compare this to our formulation of the risk-sensitive classifier as the optimization of a risk-measure (Section \ref{subsec:model:risk-sensitive:learner}). 

\subsection{Stochastic Gradient Boosting}

Beside SVMs, there are other classification techniques to consider. Random Forests and Gradient Boosting were both explored in ~\cite{lessmann_benchmarking} and \cite{brown_experimental_2012} and scored well for loan default classification.

The winning entry by Guocong Song for the Kaggle Loan Default Classification competition used a Gradient Boosting Classifier. This is a promising technique for both risk-neutral and risk-sensitive classification because it allows for the optimization of some arbitrary differentiable loss function ~\cite{_scikit-learn}. 

Friedman develops the methodology and applications of the gradient boosting machine ~\cite{friedman_greedy_2001}. In it, he describes various popular loss criteria and their applications. The mathematical definition of the Gradient Boosting (GB) algorithm is clearly described and helps us understand the usage of the Python scikit-learn package that implements the GB algorithm. We use this formulation of the Stochastic Gradient Boosting algorithm in our risk-neutral model (in Section ~\ref{sec:model:risk-neutral})

The paper also mathematically defines the ``deviance" loss function used by the default GB classifier. This loss function is a measure of the lack of fit to the data in a logistic regression model. This ``deviance" loss function is the quantity ``$-2 \cdot \mbox{the log-likelihood}$"~\cite{hastie_elements_2009}. 