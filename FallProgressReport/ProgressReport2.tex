\documentclass[12pt]{article}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage[margin=1in]{geometry}
\geometry{a4paper}
\usepackage{graphicx}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage[parfill]{parskip}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{bbm}
\usepackage{listings}
\usepackage{hyperref}
\usepackage{setspace}

\lstset{
  language=Matlab,  
  numbers=left,
  breaklines=true 
}

\setlength{\parindent}{0cm}
\parskip = \baselineskip

\newcommand{\bigO}{\ensuremath{\mathcal{O}}}% big-O notation/symbol

% tikz package for diagrams
\usepackage{tikz}
\usetikzlibrary{decorations.markings}
\usetikzlibrary{fit,positioning,shapes.geometric,arrows}
\newcommand\independent{\protect\mathpalette{\protect\independenT}{\perp}}
\def\independenT#1#2{\mathrel{\rlap{$#1#2$}\mkern2mu{#1#2}}}

\title{Loan Default Prediction: Classifying Clients for Creating Optimal Loan Portfolios}

\author{Satyajeet Pal\\\\ Professor Mengdi Wang\\ Princeton University\\Operations Research and Financial Engineering\\Senior Thesis Fall Progress Report}

\date{\today}

\begin{document}
\maketitle

\newpage

This paper represents my own work in accordance with University regulations. 
%I hereby declare that I am the sole author of this thesis.

%I authorize Princeton University to lend this thesis to other institutions or individuals for the purpose of scholarly research.

%Satyajeet Pal

%I further authorize Princeton University to reproduce this thesis by photocopying or by other means, in total or in part, at the request of other institutions or individuals for the purpose of scholarly research.

Satyajeet Pal

\newpage

\section{Abstract}

Loans are a ubiquitous part of a capitalist economy. Current methods of evaluating potential loans are dated and often require underwriters to use basic credit scores (which may be inadequate due to the bad or no credit history of most micro-loan borrowers) and simple formulas to evaluate credit risk. This thesis will attempt to create a better algorithm to predict a borrower's risk for default based on an applicants' profile, using various statistical tools to identify those borrowers who are good credit risks. 

\newpage

\tableofcontents

\doublespacing

\newpage

\section{Problem Description}

\subsection{Motivation}
Loans are a big part of a successful capitalist economy. They give individuals the opportunity to bootstrap their own startup businesses, buy homes and cars, and use credit cards. Reliable credit scoring methods are invaluable for financial institutions to evaluate loan applicants and intelligently accept new clients. 

Recent financial and debt crises, such as the 2008 subprime mortgage crisis in the US, have made banks and other financial institutions more wary of giving out loans. However, in order to continue acquiring and serving customers, these institutions must be smarter about deciding which loans to grant rather than simply granting much fewer loans. Thus, in recent years credit risk assessment and management has become an area of bigger focus. 

With improved loan evaluation, companies can not only give out loans to a higher proportion of qualified applicants but can also provide lower rates to the borrowers that they've classified as low risk. 

\subsection{Objective}
The main objective of this thesis is to help loan providers make optimal decisions when giving out loans based on applicants' profiles. This involves two separate parts: classification and optimal learning. 

\subsection{Terminology}
Before we continue, we will define some terms to make the rest of the discussion clearer. We will use the term "institution" to refer to the financial entity that issues loans to applicants. We will use the term "applicant" to refer to the individual (or entity) applying for a loan from the institution. 

\subsection{Classification}
Classification is a two-fold process. First, a binary classier identifies whether a loan is determined to be a default or non-default. The first step gives us a probability of default for an application. Using a particular threshold, yet to be discovered, we can classify each loan application as likely to result in a default or not likely to result in a default. Second, we take the loans that we feel are likely to result in a default and predict the potential loss. By establishing this classification process, a loan provider can used the train model to take any new loan application and probabilistically determine two things: whether or not the loan will default and how much the expected default is. 

\subsection{Optimal Learning}
The second part, which we will call optimal learning, is the process by which a loan provider determines which loans to accept and grant. When we create the classification model, as detailed above, we use an extensive data set with a variety of applications with varying features. However, it is possible that a given lender may not have a data set with prior examples of applicants. Thus, even if they have our model approach, they don't have data on which to train the model. This means that they must collect data by accepting loan applications. Optimizing the information gained during this process in order to train and improve the model is what we call optimal learning. 

When we train our classification model, we are given a full training set with a large variety of applicants. Thus, we can glean what type of characteristics correspond to high credit risk and low credit risk clients when we are give a full training set. However, lending corporations often have to create their own models using much smaller training sets, if at all. Thus, there is motivation for lenders to accept applicants of varying types so they can observe information about them (notably whether or not there was a default). Thus, there may be a tradeoff between choosing loans that are believed to be lowest risk and taking on some higher risk loans in order to learn more about the different types of applicants.  

Moreover, this problem is an online learning problem because the institution must accept the consequences of their decisions. That is to say that any observations of default or no-default based off an accepted loan application leads to an outcome that affects the institution. If there is no default, then there is a net gain. If there is a default, then the loss is realized by the institution and can vary. This gain or loss is ultimately part of the objective function. The institution may have an objective function of minimizing loss or keeping it under a certain threshold. So, it must carefully decide which applicants to grant loans to so that it doesn't realize too much losses in an effort to gain information. 

\newpage 

\section{Review of Literature}

The primary literature I reviewed so far was related to the classification model. 

First, we look at the models used for Probability of Default (PD) classification. Two basic techniques that were referenced time and again were Logistic Regression (LR) and Linear Discriminant Analysis (LDA). They are considered to be "relatively simple and explainable" \cite{Shi} and are often used as a benchmark for classification models to evaluate credit risk. In fact, those two techniques are considered to be the most commonly used credit scoring techniques. They end up being very competitive when compared to more complex techniques \cite{Brown}. This suggests that most credit-scoring data sets are only "weakly non-linear \cite{Baesens}. Thus, this seems like a good point to start with for our analysis. 

Support Vector Machines are often used for classification and for credit scoring models. Two different papers proposed classification models based on support vector machines and gave insight on how to improve them for the credit scoring application. One was a model for feature-weighted support vector machines \cite{Shi} while the other is a fuzzy support vector machine \cite{Fuzzy}. The feature-weighted support vector machine builds on standard SVMs by assigning different weights to different input features. This paper found that RF-FWSVM (random forest-feature weighted svm) had "high classification accuracy and good stability for practical credit scoring applications" \cite{Shi}. Thus, it is worth exploring these specialized SVMs to improve the credit scoring model. 

Finally, there are a few remaining techniques to explore based off the literature. Random Forests and Gradient Boosting were both explored in \cite{Lessmann} and \cite{Brown} and scored well for PD classification. These techniques were also used in many of the Kaggle competition submissions and are worth pursuing because they were common to some of the winning entries. 

When analyzing the effectiveness of the classification, I must be careful of the fact that credit scoring data is usually imbalanced with many more "good" applicants than "bad" applicants (in danger of default). It is suggested that a combination of the F-measure and weighted accuracy be used to score my classification models. Literature suggests that a good way to deal with imbalanced data (when using Random Forests) is to use Weighted or Balanced Random Forests \cite{RandomForestImbalanced}. I'll be exploring this as well. 

Further exploration and lit review must be performed for the second part of the classification which is finding the expected loss given default. This part of the problem is primarily a regression problem. Thus, I must explore various methods of non-linear regression to improve the model. 

Finally, the last part of the thesis, which I have yet to explore fully, will be the optimal learning problem. Review of \cite{Powell} suggested various algorithms to use for the problem. Obviously, we could use a purely exploration or exploitation algorithm. The former will try to sample applicants of as varied characteristics as possible while the exploitation algorithm will sample applicants with the lowest predicted probability of default. There are other methods, namely the Knowledge Gradient for Online Learning, that I will focus most of my attention towards as intelligent algorithms for accepting loans. 


\newpage

\section {Data}

The clearest and most available source of data is the Kaggle Loan Default Competition (\url{https://www.kaggle.com/c/loan-default-prediction}).

The Kaggle dataset is a large dataset corresponding to a set of financial transactions by individuals. It is  completely anonymized dataset and has been standardized and de-trended [Kaggle]. The data in the training set is sorted by date. In order to remove any time-dimensionality, we randomize the order of the data. 

The attributes of the borrowers is multi-dimensional and thus, part of the challenge will be deciding which features can be used for prediction. The data is a combination of categorical, discrete, and continuous variables. The dataset has approximately 200,000 observations and 800 features. Unfortunately, the attributes are unlabeled. While this doesn't necessarily affect our algorithms, it obscures the analysis because there is no economic or financial context to the features we find important.

in addition to the attributes applicants, each observation in the dataset has a record of whether or not a default occurred for that application. Whenever there was a default, the loss was also recorded with a value between 0 and 100. For example, if the loss value was 25, then that means only 75 was returned from the loan (where the loan amount is normalized to 100). 

This dataset consists of both a training and testing set. Having these separate sets will be valuable if I pursue the classification model and decision tree aspect of the loan default problem. These datasets will also be useful if I decide to instead focus more on the optimal learning problem of discovery of new borrowers. 

While the dataset is large, there are holes in the data, so a strategy will be required to fill in the gaps. It is evident from the sheer number of features that a big part of classification will be feature selection. It may also be valuable to combine features. 

An important point to be careful of when doing the classification is that typical credit scoring data is heavily imbalanced. That is, there are much fewer defaults than non-defaults in the population of loan applications. Thus, conventional classification techniques may bias heavily toward non-default and seem accurate just because there is only a small proportion of default loan applications. This was discussed in the literature review. 


\newpage 

  
\section{Summary of Project Work}

So far, all of my work has been with the Kaggle competition data set. I've been writing code primarily in R to input the training set in a reasonable format and then be able to parse and analyze it. I spent quite a bit of time trying to figure out how to work with the really large data set on my machine or try to work with it on an external server before realizing that I should just shrink the training set  (so that my machine can handle it more easily) and do initial exploration on this smaller data set. 

Part of the work was to determine what language and environment to work in. I chose to use R because it has a rich set of libraries related to machine learning. I also have quite a bit of prior experience using it, which makes it a good choice. I also considered Python and Matlab, but it seemed like there were fewer libraries to use. At some point, I may have to consider Python if the dataset is just too large to efficiently process in R. 

There has been a lot of work done on this dataset by others (due to the nature of the competition), so the next step was to read through all of the submissions and solutions. There is a significant amount of overlap in what the top solutions do, so I have been synthesizing their work to try and replicate the results. According to a forum post, an individual named Yasser posted a pair of features f527 and f528 which used in conjunction result in a high classification accuracy. In addition f274 seems promising. I've been trying to find other sets of features which lead to high classification accuracy (which many sources call "Golden Features"). 

While I haven't had a chance to do this over the past week, I hope to create a benchmark using the LDA and LR techniques as discussed in the Literature Review. This should be done by the end of this week and will provide me a good starting point for my deeper analysis. 


\newpage

\singlespacing

\begin{thebibliography}{100} % 100 is a random guess of the total number of
%references
\bibitem{CCC} Carter, Chris, and Jason Catlett. "Assessing Credit Card Applications Using Machine Learning." IEEE Expert 2, no. 3 (September 1987): 71?79. doi:10.1109/MEX.1987.4307093.
\bibitem{Huang} Huang, Cheng-Lung, Mu-Chen Chen, and Chieh-Jen Wang. "Credit Scoring with a Data Mining Approach Based on Support Vector Machines." Expert Systems with Applications 33, no. 4 (November 2007): 847?56. doi:10.1016/j.eswa.2006.07.007.
\bibitem{Ogler} Orgler, Yair E. "A Credit Scoring Model for Commercial Loans." Journal of Money, Credit and Banking 2, no. 4 (November 1, 1970): 435?45. doi:10.2307/1991095.
\bibitem{Shi} Shi, Jian, Shu-you Zhang, and Le-miao Qiu. "Credit Scoring by Feature-Weighted Support Vector Machines." Journal of Zhejiang University SCIENCE C 14, no. 3 (March 1, 2013): 197?204. doi:10.1631/jzus.C1200205.
\bibitem{Fuzzy} Wang, Yongqiao, Shouyang Wang, and K.K. Lai. "A New Fuzzy Support Vector Machine to Evaluate Credit Risk." IEEE Transactions on Fuzzy Systems 13, no. 6 (December 2005): 820?31. doi:10.1109/TFUZZ.2005.859320.
\bibitem{Lessmann} Lessmann, Stefan, Hsin-Vonn Seow, Bart Baesens, and Lyn Thomas. "Benchmarking State-of-the-Art Classification Algorithms for Credit Scoring: A Ten-Year Update,? n.d.
\bibitem{Brown} Brown, Iain, and Christophe Mues. "An Experimental Comparison of Classification Algorithms for Imbalanced Credit Scoring Data Sets." Expert Systems with Applications 39, no. 3 (2012): 3446?53.
\bibitem{Baesens} Baesens, B., T. Van Gestel, S. Viaene, M. Stepanova, J. Suykens, and J. Vanthienen. "Benchmarking State-of-the-Art Classification Algorithms for Credit Scoring." The Journal of the Operational Research Society 54, no. 6 (June 1, 2003): 627?35.
\bibitem{Neuro-Fuzzy} Pal, S. K., R. K. De, and J. Basak. "Unsupervised Feature Evaluation: A Neuro-Fuzzy Approach." IEEE Transactions on Neural Networks / a Publication of the IEEE Neural Networks Council 11, no. 2 (2000): 366?76. doi:10.1109/72.839007.
\bibitem{SVN_Neural}Huang, Zan, Hsinchun Chen, Chia-Jung Hsu, Wun-Hwa Chen, and Soushan Wu. ?Credit Rating Analysis with Support Vector Machines and Neural Networks: A Market Comparative Study.? Decision Support Systems, Data mining for financial decision making, 37, no. 4 (September 2004): 543?58. doi:10.1016/S0167-9236(03)00086-1.
\bibitem{RandomForestImbalanced} Chen, Chao, Andy Liaw, and Leo Breiman. Using Random Forest to Learn Imbalanced Data. University of California at Berkeley, Berkeley, California: Statistics Department, University of California, Berkeley, 2004. Web. Statistics Technical Reports.
\bibitem{Saranya} Saranya, R., and C. Yamini. "Survey on Ensemble Alpha Tree for Imbalance Classification Problem." n. pag. Google Scholar. Web. 10 Nov. 2014.
\bibitem{Powell} Warren B. Powell, Ilya O. Ryzhov. "Optimal Learning".
\bibitem{Kaggle} https://www.kaggle.com/c/loan-default-prediction/data
\end{thebibliography}


\end{document}