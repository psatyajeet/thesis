install.packages("foreach")
install.packages("gbm")
install.packages("snow")
install.packages("doSNOW")
install.packages("verification")
install.packages("ROCR")
install.packages("caret")

library("plyr")
library("foreach")
library("gbm")
library("snow")
library("doSNOW")
library("verification")
library("reshape2")
library("ROCR")
library("caret")
library("MASS")

setwd("/Users/satyajeetpal/spal/Princeton/Thesis/")

mae = function(x,y) { mean(abs(x-y)) }

train = read.csv("Data/train_v2.csv", stringsAsFactors = F, header = T)

test = read.csv("Data/test_v2.csv", stringsAsFactors = F, header = T)
test$loss = NA

## combine train and test set to one complete table for easier handling ##
data = rbind(train, test)

## remove duplicate columns based on the first 25000 rows ##
data = data[!duplicated(as.list(data[1:25000,]))]

## correct f4 ##
data$f4 = data$f4/100

## save copy for faster loading ##
save(data, file = "final_data_full.RData")

load("final_data_full.RData")

data = data[1:10000,]

for(i in colnames(data)){ 
  data$default = (data$loss > 0)*1 
}

# lda (default ~ ., data=data)


## build a correlation matrix based on the first 100000 rows ##
corr.matrix = cor(data[1:100000, 2:679], use = "pairwise.complete.obs")
corr.matrix[is.na(corr.matrix)] = 0

## create sets of features that are very highly correlated ##
corr.list = foreach(i = 1:nrow(corr.matrix)) %do% {
  rownames(corr.matrix[corr.matrix[,i] > 0.996,])
}

## remove empty sets ##
corr.list = corr.list[sapply(corr.list, function(x) length(x) > 0 )]

## remove duplicated sets ##
corr.list = unique(corr.list)

## create dataframe of correlated pairs of features ##
corr.pairs = foreach(i = 1:length(corr.list), .combine = rbind) %do% {
  
  temp.feats = corr.list[[i]]
  
  t(combn(temp.feats,2))
  
}

## remove duplicated pairs ##
corr.pairs = unique(corr.pairs)

## compute difference of each feature pair and save as dataframe ##
golden.features = foreach(i = 1:length(corr.pairs[,1]), .combine = cbind) %do% {  
  temp.feats = corr.pairs[i,]
  new.feat.temp = data[,temp.feats[1]] - data[,temp.feats[2]]
}
golden.features = as.data.frame(golden.features)

## create useful column names ##
colnames(golden.features) = apply(corr.pairs, 1, function(x) paste("diff", x[1], x[2], sep = "_"))

## save golden features ##
save(golden.features, file = "final_golden_features.RData")
load("final_golden_features.RData")

## lots of these golden features are very highly correlated and can be removed to save space and time ## 
## remove correlated features based on the first 100000 rows ##

## create correlation matrix
corr.matrix = cor(golden.features[1:100000,], use = "pairwise.complete.obs")
corr.matrix[is.na(corr.matrix) == T] = 0

## find highly correlated features ##
useless.features = colnames(corr.matrix)[findCorrelation(corr.matrix, cutoff = 0.99, verbose = F)]

## remove highly correlated features ##
for(i in useless.features) {golden.features[,i] = NULL}
golden.features = golden.features[,!(colnames(golden.features) %in% useless.features)]

## save golden features ##
save(golden.features, file = "final_golden_features.RData")
load("final_golden_features.RData")

