import pandas as pd
import numpy as np
import os
import sys
from functools import partial
import models
import train_predict
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler, Imputer
from sklearn.pipeline import Pipeline
from sklearn.ensemble import GradientBoostingClassifier, GradientBoostingRegressor


from sklearn.linear_model import LogisticRegression
from sklearn.cross_validation import train_test_split
from costcla.datasets import load_creditscoring2, load_creditscoring1
from costcla.models import CostSensitiveLogisticRegression
from costcla.metrics import savings_score, cost_loss, binary_classification_metrics

import matplotlib.pyplot as plt
from sklearn.utils import column_or_1d

data = load_creditscoring1()

sets = train_test_split(data.data, data.target, data.cost_mat, test_size=0.33, random_state=0)
X_train, X_test, y_train, y_test, cost_mat_train, cost_mat_test = sets
y_pred_test_lr = LogisticRegression(random_state=0).fit(X_train, y_train).predict(X_test)
f = CostSensitiveLogisticRegression(verbose=1)
f.fit(X_train, y_train, cost_mat_train)
y_pred_test_cslr = f.predict(X_test)
y_pred_test_cslr_proba = f.predict_proba(X_test)


costs = find_costs(y_train, y_pred_train_cslr, cost_mat_train)

cost_df= pd.DataFrame(costs, columns = ['cost']) 

VaR = cost_df.quantile(quantile)



# Savings using Logistic Regression
print savings_score(y_test, y_pred_test_lr, cost_mat_test)

# Savings using Cost Sensitive Logistic Regression
print savings_score(y_test, y_pred_test_cslr, cost_mat_test)

print cost_loss(y_test, y_pred_test_lr, cost_mat_test)
print cost_loss(y_test, y_pred_test_cslr, cost_mat_test)


#############################################################################

data = load_creditscoring2()

sets = train_test_split(data.data, data.target, data.cost_mat, test_size=0.33, random_state=0)
X_train, X_test, y_train, y_test, cost_mat_train, cost_mat_test = sets
y_pred_test_lr = LogisticRegression(random_state=0).fit(X_train, y_train).predict(X_test)
f = CostSensitiveLogisticRegression(verbose=0)

def subtract_alpha(x, alpha):
    return max(x - alpha, 0)

def find_costs(y_true, y_pred, cost_mat):
    y_true = column_or_1d(y_true)
    y_pred = column_or_1d(y_pred)
    cost = y_true * ((1 - y_pred) * cost_mat[:, 1] + y_pred * cost_mat[:, 2])
    cost += (1 - y_true) * (y_pred * cost_mat[:, 0] + (1 - y_pred) * cost_mat[:, 3])
    return cost

def find_cost_over_var(y_true, y_pred, cost_mat, alpha):
    y_true = column_or_1d(y_true)
    y_pred = column_or_1d(y_pred)
    cost = y_true * ((1 - y_pred) * cost_mat[:, 1] + y_pred * cost_mat[:, 2])
    cost += (1 - y_true) * (y_pred * cost_mat[:, 0] + (1 - y_pred) * cost_mat[:, 3])
    return vector_subtract_alpha(cost, alpha)


vector_subtract_alpha = np.vectorize(subtract_alpha)

quantile=0.95

# initial alpha
alpha = 0
length = np.size(y_train)


############ LOOOOPPPP #################


# update modified cost
current_cost = vector_subtract_alpha(cost_mat_train, alpha)

# find optimal parameters
f.fit(X_train, y_train, current_cost)

# calculate new costs
y_pred_train_cslr = f.predict(X_train)
y_pred_train_cslr_proba = f.predict_proba(X_train)

costs = find_costs(y_train, y_pred_train_cslr, cost_mat_train)

cost_df= pd.DataFrame(costs, columns = ['cost']) 

VaR = cost_df.quantile(quantile)

alpha = VaR

cVaR = alpha + (1/((1-quantile)*(length)))*np.sum(find_cost_over_var(y_train, y_pred_train_cslr, cost_mat_train, alpha))

print alpha
print cVaR

'''
fig = plt.figure()
ax = fig.add_subplot(111)
'''

'''
numBins = 10
ax.hist(costs,numBins,color='green',alpha=0.8)
plt.show()
'''

np.savetxt('file.txt', np.sort(costs))








