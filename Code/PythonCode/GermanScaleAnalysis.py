import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.cross_validation import train_test_split, KFold, StratifiedKFold

from costcla.models import CostSensitiveLogisticRegression
from costcla.metrics import savings_score, cost_loss, binary_classification_metrics

from sklearn.utils import column_or_1d

class Bunch(dict):
    """Container object for datasets: dictionary-like object that
       exposes its keys as attributes."""
    def __init__(self, **kwargs):
        dict.__init__(self, kwargs)
        self.__dict__ = self

def subtract_alpha(x, alpha):
    return max(x - alpha, 0)

def find_costs(y_true, y_pred, cost_mat):
    y_true = column_or_1d(y_true)
    y_pred = column_or_1d(y_pred)
    cost = y_true * ((1 - y_pred) * cost_mat[:, 1] + y_pred * cost_mat[:, 2])
    cost += (1 - y_true) * (y_pred * cost_mat[:, 0] + (1 - y_pred) * cost_mat[:, 3])
    return cost

def find_cost_over_var(y_true, y_pred, cost_mat, alpha):
    y_true = column_or_1d(y_true)
    y_pred = column_or_1d(y_pred)
    cost = y_true * ((1 - y_pred) * cost_mat[:, 1] + y_pred * cost_mat[:, 2])
    cost += (1 - y_true) * (y_pred * cost_mat[:, 0] + (1 - y_pred) * cost_mat[:, 3])
    return vector_subtract_alpha(cost, alpha)

def load_data(name):
    raw_data = np.loadtxt(name)
    n_samples = raw_data.shape[0]
    data = raw_data[:, :-1]
    target = raw_data[:, -1] - 1
    cost_mat = np.zeros((n_samples, 4))  # cost_mat[FP,FN,TP,TN]
    #FP = interest, predict 1, actual 0
    cost_mat[:, 0] = 0.1 * (data[:, 1]/12) * data[:, 3]
    #FN = loss given default, predict 0, actual 1
    cost_mat[:, 1] = 0.30 * data[:, 3]
    cost_mat[:, 2] = 0.0
    cost_mat[:, 3] = 0.0
    return Bunch(data= data, target=target, cost_mat=cost_mat)

def confirm_var(var, data, length):
    count = 0
    sum = 0
    for datum in data:
        if datum >= np.asscalar(var): 
            count = count + 1
            sum = sum + datum

    amount = count/float(length)
    expected = sum/float(length)
    return amount, expected 


vector_subtract_alpha = np.vectorize(subtract_alpha)


data = load_data('german.data-numeric.txt')

skf = StratifiedKFold(data.target, 2)
#sets = train_test_split(data.data, data.target, data.cost_mat, test_size=0.33, random_state=0)

cs_results = np.empty((2), dtype=object)
rs_results = np.empty((2, 51), dtype=object)

for n, (train, test) in enumerate(skf):

    #######################################
    #X_train, X_test, y_train, y_test, cost_mat_train, cost_mat_test = sets
    X_train, X_test, y_train, y_test, cost_mat_train, cost_mat_test = data.data[train], data.data[test], data.target[train], data.target[test], data.cost_mat[train], data.cost_mat[test] 

    quantile = 0.95

    combos = [0 for i in range(51)]

    length = y_train.shape[0]

    for j in xrange(51):

        alpha = 0
        nu = 0.05 * j
        combos[j] = CostSensitiveLogisticRegression(solver='ga') # 'bfgs' or 'ga'

        for i in xrange(51):
            # update modified cost
            current_cost = nu * cost_mat_train + (1-nu) * vector_subtract_alpha(cost_mat_train, alpha)

            # find optimal parameters
            
            combos[j].fit(X_train, y_train, current_cost)

            # calculate new costs
            y_pred_train_cslr = combos[j].predict(X_train)
            # name = "GermanDataSet_Iteration%d" % i
            # y_pred_train_cslr_proba = combos[j].predict_proba(X_train)
            # np.savetxt(name, y_pred_train_cslr)
            # print cost_loss(y_train, y_pred_train_cslr, current_cost)

            costs = find_costs(y_train, y_pred_train_cslr, cost_mat_train)

            cost_df= pd.DataFrame(costs, columns = ['cost']) 

            VaR = cost_df.quantile(quantile)    
            cVaR = alpha + (1/((1-quantile)*(length)))*np.sum(find_cost_over_var(y_train, y_pred_train_cslr, cost_mat_train, alpha))

            # v, s =  confirm_var(VaR, cost_df['cost'], length)

            # print "%d\t\t%f\t%f\t\t%f" % (i, alpha, cVaR, sum(costs))
            # alpha = VaR
            alpha = VaR
        print ""


    ########### test sets

    #lr = LogisticRegression(random_state=0).fit(X_train, y_train)
    cs = CostSensitiveLogisticRegression(solver='ga') # 'bfgs' or 'ga'
    cs.fit(X_train, y_train, cost_mat_train)


    ## CSLR
    y_pred_test_cslr = cs.predict(X_test)
    name = "GermanDataSet_CLSR%d" % i
    # y_pred_test_rslr_proba = rs.predict_proba(X_test)
    np.savetxt(name, y_pred_test_cslr)
    # print cost_loss(y_train, y_pred_train_cslr, current_cost)

    costs = find_costs(y_test, y_pred_test_cslr, cost_mat_test)
    cost_df= pd.DataFrame(costs, columns = ['cost']) 

    length = y_test.shape[0]

    VaR = cost_df.quantile(quantile)    
    cVaR = VaR + (1/((1-quantile)*(length)))*np.sum(find_cost_over_var(y_test, y_pred_test_cslr, cost_mat_test, VaR))

    #v, s =  confirm_var(VaR, cost_df['cost'], length)

    cs_results[n] = Bunch(ratio=j, var=VaR, cvar = cVaR, cost = sum(costs))
    #print "CSLR:\t\t%f\t\t%f\t%f\t\t%f" % (j, VaR, cVaR, sum(costs))
    #print ""

    for j in xrange(51):

        ## Combo
        y_pred_test_rslr = combos[j].predict(X_test)
        name = "GermanDataSet_Combo%d" % j
        # y_pred_test_rslr_proba = rs.predict_proba(X_test)
        # np.savetxt(name, y_pred_test_rslr)
        # print cost_loss(y_train, y_pred_train_cslr, current_cost)

        costs = find_costs(y_test, y_pred_test_rslr, cost_mat_test)
        cost_df= pd.DataFrame(costs, columns = ['cost']) 

        length = y_test.shape[0]

        VaR = cost_df.quantile(quantile)    
        cVaR = VaR + (1/((1-quantile)*(length)))*np.sum(find_cost_over_var(y_test, y_pred_test_rslr, cost_mat_test, VaR))

        #v, s =  confirm_var(VaR, cost_df['cost'], length)

        rs_results[n][j] = Bunch(ratio=j/50.0, var=VaR, cvar = cVaR, cost = sum(costs))
        #print "Combo:\t\t%f\t\t%f\t%f\t\t%f" % (j/50.0, VaR, cVaR, sum(costs))
        #print ""


#print cs_results
#print rs_results

print "CSLR:\t\t%f\t\t%f\t%f\t\t%f" % (np.mean([r.ratio for r in cs_results[:]]), np.mean([r.var for r in cs_results[:]]), np.mean([r.cvar for r in cs_results[:]]), np.mean([r.cost for r in cs_results[:]]))
for i in xrange(51):
        print "RSLR:\t\t%f\t\t%f\t%f\t\t%f" % (np.mean([r.ratio for r in rs_results[:, i]]), np.mean([r.var for r in rs_results[:, i]]), np.mean([r.cvar for r in rs_results[:, i]]), np.mean([r.cost for r in rs_results[:, i]]))        

