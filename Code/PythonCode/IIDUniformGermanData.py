import pandas as pd
import numpy as np
import random
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.cross_validation import train_test_split, KFold, StratifiedKFold

from costcla.models import CostSensitiveLogisticRegression
from costcla.metrics import savings_score, cost_loss, binary_classification_metrics

from sklearn.utils import column_or_1d

class Bunch(dict):
    """Container object for datasets: dictionary-like object that
       exposes its keys as attributes."""
    def __init__(self, **kwargs):
        dict.__init__(self, kwargs)
        self.__dict__ = self

def subtract_alpha(x, alpha):
    return max(x - alpha, 0)

vector_subtract_alpha = np.vectorize(subtract_alpha)

def find_costs(y_true, y_pred, cost_mat):
    y_true = column_or_1d(y_true)
    y_pred = column_or_1d(y_pred)
    cost = y_true * ((1 - y_pred) * cost_mat[:, 1] + y_pred * cost_mat[:, 2])
    cost += (1 - y_true) * (y_pred * cost_mat[:, 0] + (1 - y_pred) * cost_mat[:, 3])
    return cost

def find_cost_over_var(y_true, y_pred, cost_mat, alpha):
    y_true = column_or_1d(y_true)
    y_pred = column_or_1d(y_pred)
    cost = y_true * ((1 - y_pred) * cost_mat[:, 1] + y_pred * cost_mat[:, 2])
    cost += (1 - y_true) * (y_pred * cost_mat[:, 0] + (1 - y_pred) * cost_mat[:, 3])
    return vector_subtract_alpha(cost, alpha)

def load_data(name):
    raw_data = np.loadtxt(name)
    n_samples = raw_data.shape[0]
    data = raw_data[:, :-1]
    #target = raw_data[:, -1] - 1
    #target = np.random.uniform(0, 1, n_samples)
    target = np.array([random.randint(0,1) for i in range(n_samples)])
    cost_mat = np.zeros((n_samples, 4))  # cost_mat[FP,FN,TP,TN]
    #FP = interest, predict 1, actual 0
    cost_mat[:, 0] = 0.1 * (data[:, 1]/12) * data[:, 3]
    #FN = loss given default, predict 0, actual 1
    cost_mat[:, 1] = 0.30 * data[:, 3]
    cost_mat[:, 2] = 0.0
    cost_mat[:, 3] = 0.0
    return Bunch(data= data, target=target, cost_mat=cost_mat)

def get_averages(bunch):
    avg_var = np.mean(bunch.var)
    avg_cvar = np.mean(bunch.cvar)
    avg_cost = np.mean(bunch.cost)
    return Bunch(quantile=bunch.quantile, var=avg_var, cvar = avg_cvar, cost = avg_cost)

def confirm_var(var, data, length):
    count = 0
    sum = 0
    for datum in data:
        if datum >= np.asscalar(var): 
            count = count + 1
            sum = sum + datum

    amount = count/float(length)
    expected = sum/float(length)
    return amount, expected 

def feature_selection():
    data = load_data('german.data-numeric.txt')
    sets = train_test_split(data.data, data.target, data.cost_mat, test_size=0.33, random_state=0)
    X_train, X_test, y_train, y_test, cost_mat_train, cost_mat_test = sets
    clf =   GradientBoostingClassifier(
                       loss='deviance', learning_rate=0.01, n_estimators=3000,
                       subsample=0.6, min_samples_split=12, min_samples_leaf=12,
                       max_depth=6, random_state=1357, verbose=1)
    clf.fit(X_train, y_train)
    clf.predict(X_test)
    print clf.score(X_test, y_test)

    print clf.feature_importances_

    plt.bar(np.arange(24), clf.feature_importances_, align='center')
    plt.show()


def run_risk_sensitive():

    vector_subtract_alpha = np.vectorize(subtract_alpha)

    data = load_data('german.data-numeric.txt')

    skf = StratifiedKFold(data.target, 5)

    lr_results = np.empty((5, 2), dtype=object)
    cs_results = np.empty((5, 2), dtype=object)
    rs_results = np.empty((5, 2, 2), dtype=object)

    for n, (train, test) in enumerate(skf):

        #sets = train_test_split(data.data, data.target, data.cost_mat, test_size=0.33, random_state=2)
        #X_train, X_test, y_train, y_test, cost_mat_train, cost_mat_test = sets
        X_train, X_test, y_train, y_test, cost_mat_train, cost_mat_test = data.data[train], data.data[test], data.target[train], data.target[test], data.cost_mat[train], data.cost_mat[test] 

        # initial alpha
        alpha = 0
        length = y_train.shape[0]

        quantiles = [0.90, 0.95]
        rs = [0 for i in xrange(2)]
        #quantiles = [0.80, 0.90, 0.95, 0.99]
        

        for j, quantile in enumerate(quantiles):

            rs[j] = CostSensitiveLogisticRegression(solver='ga') # 'bfgs' or 'ga'
            max_cVaR = 99999999999

            for i in xrange(30):
                # update modified cost
                current_cost = vector_subtract_alpha(cost_mat_train, alpha)

                # find optimal parameters
                
                rs[j].fit(X_train, y_train, current_cost)

                # calculate new costs
                y_pred_train_cslr = rs[j].predict(X_train)
                # name = "GermanDataSet_Iteration%d" % i
                # y_pred_train_cslr_proba = rs[j].predict_proba(X_train)
                # np.savetxt(name, y_pred_train_cslr)
                # print cost_loss(y_train, y_pred_train_cslr, current_cost)

                costs = find_costs(y_train, y_pred_train_cslr, cost_mat_train)

                cost_df= pd.DataFrame(costs, columns = ['cost']) 

                VaR = cost_df.quantile(quantile)    
                cVaR = alpha + (1/((1-quantile)*(length)))*np.sum(find_cost_over_var(y_train, y_pred_train_cslr, cost_mat_train, alpha))

                #if np.asscalar(cVaR) < max_cVaR:
                max_cVaR = np.asscalar(cVaR)
                alpha = (alpha + VaR)/2
                # print "%d\t\t%f\t%f" % (i, alpha, cVaR)
                # v, s =  confirm_var(VaR, cost_df['cost'], length)               

            print ""

        ########### test sets

        lr_cost_mat = np.zeros((y_train.shape[0], 4))  # cost_mat[FP,FN,TP,TN]
        #FP = interest, predict 1, actual 0
        lr_cost_mat[:, 0] = 1
        #FN = loss given default, predict 0, actual 1
        lr_cost_mat[:, 1] = 1
        lr_cost_mat[:, 2] = 0.0
        lr_cost_mat[:, 3] = 0.0
        lr = CostSensitiveLogisticRegression(solver='ga')
        lr.fit(X_train, y_train, lr_cost_mat)
        cs = CostSensitiveLogisticRegression(solver='ga') # 'bfgs' or 'ga'
        cs.fit(X_train, y_train, cost_mat_train)

        #quantiles = [0.80, 0.90, 0.95, 0.99]
        quantiles = [0.90, 0.95]

        for m, quantile in enumerate(quantiles):

            ## BASIC LR
            y_pred_test_lr = lr.predict(X_test)
            costs = find_costs(y_test, y_pred_test_lr, cost_mat_test)
            cost_df= pd.DataFrame(costs, columns = ['cost']) 

            length = y_test.shape[0]

            VaR = cost_df.quantile(quantile)
            cVaR = VaR + (1/((1-quantile)*(length)))*np.sum(find_cost_over_var(y_test, y_pred_test_lr, cost_mat_test, VaR))

            lr_results[n][m] = Bunch(quantile=quantile, var=VaR, cvar = cVaR, cost = sum(costs))
            #print "LR:\t\t%f\t\t%f\t%f\t\t%f" % (quantile, VaR, cVaR, sum(costs))

            ## CSLR
            y_pred_test_cslr = cs.predict(X_test)
            #y_pred_test_cslr_proba = cs.predict_proba(X_test)

            costs = find_costs(y_test, y_pred_test_cslr, cost_mat_test)
            cost_df= pd.DataFrame(costs, columns = ['cost']) 

            length = y_test.shape[0]

            VaR = cost_df.quantile(quantile)
            cVaR = VaR + (1/((1-quantile)*(length)))*np.sum(find_cost_over_var(y_test, y_pred_test_cslr, cost_mat_test, VaR))

            cs_results[n][m] = Bunch(quantile=quantile, var=VaR, cvar = cVaR, cost = sum(costs))
            #print "CSLR:\t\t%f\t\t%f\t%f\t\t%f" % (quantile, VaR, cVaR, sum(costs))

            ## RSLR
            for q, rs_solver in enumerate(rs):
                y_pred_test_rslr = rs_solver.predict(X_test)
                # name = "GermanDataSet_Iteration%d" % i
                #y_pred_test_rslr_proba = rs.predict_proba(X_test)
                # np.savetxt(name, y_pred_train_cslr)
                # print cost_loss(y_train, y_pred_train_cslr, current_cost)

                costs = find_costs(y_test, y_pred_test_rslr, cost_mat_test)
                cost_df= pd.DataFrame(costs, columns = ['cost']) 

                length = y_test.shape[0]

                VaR = cost_df.quantile(quantile)    
                cVaR = VaR + (1/((1-quantile)*length))*np.sum(find_cost_over_var(y_test, y_pred_test_rslr, cost_mat_test, VaR))

                #v, s =  confirm_var(VaR, cost_df['cost'], length)
                rs_results[n][m][q] = Bunch(quantile=quantile, var=VaR, cvar = cVaR, cost = sum(costs))
                #print "RSLR:\t\t%f\t\t%f\t%f\t\t%f" % (quantile, VaR, cVaR, sum(costs))

            #print ""

    # print lr_results

    # print cs_results
    # print rs_results

    #tested in [0.8, 0.9, 0.95, 0.99]
    for i in xrange(2):
        print "LR:\t\t%f\t\t%f\t%f\t\t%f" % (np.mean([r.quantile for r in lr_results[:, i]]), np.mean([r.var for r in lr_results[:, i]]), np.mean([r.cvar for r in lr_results[:, i]]), np.mean([r.cost for r in lr_results[:, i]]))
        print "CSLR:\t\t%f\t\t%f\t%f\t\t%f" % (np.mean([r.quantile for r in cs_results[:, i]]), np.mean([r.var for r in cs_results[:, i]]), np.mean([r.cvar for r in cs_results[:, i]]), np.mean([r.cost for r in cs_results[:, i]]))
        for j in xrange(2):
            print "RSLR:\t\t%f\t\t%f\t%f\t\t%f" % (np.mean([r.quantile for r in rs_results[:, i, j]]), np.mean([r.var for r in rs_results[:, i, j]]), np.mean([r.cvar for r in rs_results[:, i, j]]), np.mean([r.cost for r in rs_results[:, i, j]]))        
        print

if __name__ == '__main__':
    run_risk_sensitive()