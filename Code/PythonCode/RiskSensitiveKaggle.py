import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
from sklearn.cross_validation import train_test_split

from costcla.models import CostSensitiveLogisticRegression
from costcla.metrics import savings_score, cost_loss, binary_classification_metrics

from sklearn.utils import column_or_1d

class Bunch(dict):
    """Container object for datasets: dictionary-like object that
       exposes its keys as attributes."""
    def __init__(self, **kwargs):
        dict.__init__(self, kwargs)
        self.__dict__ = self

def subtract_alpha(x, alpha):
    return max(x - alpha, 0)

def calculate_binary_loss(loss):
    return 1 if loss > 0 else 0

def find_costs(y_true, y_pred, cost_mat):
    y_true = column_or_1d(y_true)
    y_pred = column_or_1d(y_pred)
    cost = y_true * ((1 - y_pred) * cost_mat[:, 1] + y_pred * cost_mat[:, 2])
    cost += (1 - y_true) * (y_pred * cost_mat[:, 0] + (1 - y_pred) * cost_mat[:, 3])
    return cost

def find_cost_over_var(y_true, y_pred, cost_mat, alpha):
    y_true = column_or_1d(y_true)
    y_pred = column_or_1d(y_pred)
    cost = y_true * ((1 - y_pred) * cost_mat[:, 1] + y_pred * cost_mat[:, 2])
    cost += (1 - y_true) * (y_pred * cost_mat[:, 0] + (1 - y_pred) * cost_mat[:, 3])
    return vector_subtract_alpha(cost, alpha)

def load_data(name):
    raw_data = pd.read_csv(name, na_values=['NA', 'na'], sep=',')
    raw_data = raw_data.dropna()
    n_samples = raw_data.shape[0]
    data = raw_data.values[:, :-1]
    print data
    loss = raw_data.values[:, -1]
    vector_calculate_binary_loss = np.vectorize(calculate_binary_loss)
    target = vector_calculate_binary_loss(loss)

    cost_mat = np.zeros((n_samples, 4))  # cost_mat[FP,FN,TP,TN]
    # FP = interest, predict 1, actual 0
    cost_mat[:, 0] = .7 # * (data[:, 1]/12) * data[:, 3]
    # FN = loss given default, predict 0, actual 1
    cost_mat[:, 1] = .1 # * data[:, 3]

    # TP and TN
    cost_mat[:, 2] = 0.0
    cost_mat[:, 3] = 0.0
    return Bunch(data= data, target=target, cost_mat=cost_mat)

vector_subtract_alpha = np.vectorize(subtract_alpha)

data = load_data('train_v2_small.csv')

'''
sets = train_test_split(data.data, data.target, data.cost_mat, test_size=0.33, random_state=0)
X_train, X_test, y_train, y_test, cost_mat_train, cost_mat_test = sets


y_pred_test_lr = LogisticRegression(random_state=0).fit(X_train, y_train).predict(X_test)

costs = find_costs(y_test, y_pred_test_lr, cost_mat_test)
cost_df= pd.DataFrame(costs, columns = ['cost']) 
quantile=0.95
alpha = 0
length = y_test.shape[0]

np.savetxt('first_true', y_test)
np.savetxt('first_pred', y_pred_test_lr)

VaR = cost_df.quantile(quantile)

cVaR = VaR + (1/((1-quantile)*(length)))*np.sum(find_cost_over_var(y_test, y_pred_test_lr, cost_mat_test, VaR))

print np.asscalar(VaR)
print np.asscalar(cVaR)

f = CostSensitiveLogisticRegression(solver='ga') # 'bfgs' or 'ga'
f.fit(X_train, y_train, cost_mat_train)

y_pred_test_cslr = f.predict(X_test)
# y_pred_test_cslr_proba = f.predict_proba(X_test)

costs = find_costs(y_test, y_pred_test_cslr, cost_mat_test)
cost_df= pd.DataFrame(costs, columns = ['cost']) 
quantile=0.95
alpha = 0
length = y_test.shape[0]

np.savetxt('second_true', y_test)
np.savetxt('second_pred', y_pred_test_cslr)

VaR = cost_df.quantile(quantile)

cVaR = VaR + (1/((1-quantile)*(length)))*np.sum(find_cost_over_var(y_test, y_pred_test_cslr, cost_mat_test, VaR))

print np.asscalar(VaR)
print np.asscalar(cVaR)


##############################################

sets = train_test_split(data.data, data.target, data.cost_mat, test_size=0.90, random_state=0)
X_train, X_test, y_train, y_test, cost_mat_train, cost_mat_test = sets

def confirm_var(var, data, length):
    count = 0
    sum = 0
    for datum in data:
        if datum >= np.asscalar(var): 
            count = count + 1
            sum = sum + datum

    amount = count/float(length)
    expected = sum/float(length)
    return amount, expected 

quantile=0.95

# initial alpha
alpha = 0
length = y_train.shape[0]


print "Iteration\tAlpha\t\tcVaR"
print "-----------------------------------"
############ Loop #################

for i in xrange(50):
    # update modified cost
    current_cost = vector_subtract_alpha(cost_mat_train, alpha)

    # find optimal parameters
    f = CostSensitiveLogisticRegression(solver='bfgs', max_iter=400) # 'bfgs' or 'ga'
    f.fit(X_train, y_train, current_cost)

    # calculate new costs
    y_pred_train_cslr = f.predict(X_train)
    name = "Iteration%d" % i
    y_pred_train_cslr_proba = f.predict_proba(X_train)
    np.savetxt(name, y_pred_train_cslr)
    # print cost_loss(y_train, y_pred_train_cslr, current_cost)

    costs = find_costs(y_train, y_pred_train_cslr, cost_mat_train)

    cost_df= pd.DataFrame(costs, columns = ['cost']) 

    VaR = cost_df.quantile(quantile)    
    cVaR = alpha + (1/((1-quantile)*(length)))*np.sum(find_cost_over_var(y_train, y_pred_train_cslr, cost_mat_train, alpha))

    #v, s =  confirm_var(VaR, cost_df['cost'], length)

    print "%d\t\t%f\t%f" % (i, alpha, cVaR)
    alpha = (alpha + VaR)/2

'''

sets = train_test_split(data.data, data.target, data.cost_mat, test_size=0.33, random_state=2)
X_train, X_test, y_train, y_test, cost_mat_train, cost_mat_test = sets

# initial alpha
alpha = 0
length = y_train.shape[0]

rs = [0 for i in range(4)]
quantiles = [0.80, 0.90, 0.95, 0.99]

for j in xrange(4):

    rs[j] = CostSensitiveLogisticRegression(solver='bfgs') # 'bfgs' or 'ga'
    quantile = quantiles[j]
    max_cVaR = 99999999999

    for i in xrange(30):
        # update modified cost
        current_cost = vector_subtract_alpha(cost_mat_train, alpha)

        # find optimal parameters
        
        rs[j].fit(X_train, y_train, current_cost)

        # calculate new costs
        y_pred_train_cslr = rs[j].predict(X_train)
        # name = "GermanDataSet_Iteration%d" % i
        # y_pred_train_cslr_proba = rs[j].predict_proba(X_train)
        # np.savetxt(name, y_pred_train_cslr)
        # print cost_loss(y_train, y_pred_train_cslr, current_cost)

        costs = find_costs(y_train, y_pred_train_cslr, cost_mat_train)

        cost_df= pd.DataFrame(costs, columns = ['cost']) 

        VaR = cost_df.quantile(quantile)    
        cVaR = alpha + (1/((1-quantile)*(length)))*np.sum(find_cost_over_var(y_train, y_pred_train_cslr, cost_mat_train, alpha))

        #if np.asscalar(cVaR) < max_cVaR:
        max_cVaR = np.asscalar(cVaR)
        alpha = (alpha + VaR)/2
        print "%d\t\t%f\t%f" % (i, alpha, cVaR)
        # v, s =  confirm_var(VaR, cost_df['cost'], length)

        
        

    print ""

########### test sets

lr_cost_mat = np.zeros((y_train.shape[0], 4))  # cost_mat[FP,FN,TP,TN]
#FP = interest, predict 1, actual 0
lr_cost_mat[:, 0] = 1
#FN = loss given default, predict 0, actual 1
lr_cost_mat[:, 1] = 1
lr_cost_mat[:, 2] = 0.0
lr_cost_mat[:, 3] = 0.0
lr = CostSensitiveLogisticRegression(solver='bfgs', max_iter=400)
lr.fit(X_train, y_train, lr_cost_mat)
cs = CostSensitiveLogisticRegression(solver='bfgs', max_iter=400) # 'bfgs' or 'ga'
cs.fit(X_train, y_train, cost_mat_train)

quantiles = [0.80, 0.90, 0.95, 0.99]

for quantile in quantiles:

    ## BASIC LR
    y_pred_test_lr = lr.predict(X_test)
    costs = find_costs(y_test, y_pred_test_lr, cost_mat_test)
    cost_df= pd.DataFrame(costs, columns = ['cost']) 

    length = y_test.shape[0]

    VaR = cost_df.quantile(quantile)
    cVaR = VaR + (1/((1-quantile)*(length)))*np.sum(find_cost_over_var(y_test, y_pred_test_lr, cost_mat_test, VaR))

    print "LR:\t\t%f\t\t%f\t%f\t\t%f" % (quantile, VaR, cVaR, sum(costs))

    ## CSLR
    y_pred_test_cslr = cs.predict(X_test)
    #y_pred_test_cslr_proba = cs.predict_proba(X_test)

    costs = find_costs(y_test, y_pred_test_cslr, cost_mat_test)
    cost_df= pd.DataFrame(costs, columns = ['cost']) 

    length = y_test.shape[0]

    VaR = cost_df.quantile(quantile)
    cVaR = VaR + (1/((1-quantile)*(length)))*np.sum(find_cost_over_var(y_test, y_pred_test_cslr, cost_mat_test, VaR))

    print "CSLR:\t\t%f\t\t%f\t%f\t\t%f" % (quantile, VaR, cVaR, sum(costs))

    ## RSLR
    for rs_solver in rs:
        y_pred_test_rslr = rs_solver.predict(X_test)
        # name = "GermanDataSet_Iteration%d" % i
        #y_pred_test_rslr_proba = rs.predict_proba(X_test)
        # np.savetxt(name, y_pred_train_cslr)
        # print cost_loss(y_train, y_pred_train_cslr, current_cost)

        costs = find_costs(y_test, y_pred_test_rslr, cost_mat_test)
        cost_df= pd.DataFrame(costs, columns = ['cost']) 

        length = y_test.shape[0]

        VaR = cost_df.quantile(quantile)    
        cVaR = VaR + (1/((1-quantile)*length))*np.sum(find_cost_over_var(y_test, y_pred_test_rslr, cost_mat_test, VaR))

        #v, s =  confirm_var(VaR, cost_df['cost'], length)

        print "RSLR:\t\t%f\t\t%f\t%f\t\t%f" % (quantile, VaR, cVaR, sum(costs))

    print ""







