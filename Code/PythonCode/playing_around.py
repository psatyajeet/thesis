import pandas as pd
import numpy as np
import os
import sys
from functools import partial
import models
import train_predict
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler, Imputer
from sklearn.pipeline import Pipeline
from sklearn.ensemble import GradientBoostingClassifier, GradientBoostingRegressor


read_csv = partial(pd.read_csv, na_values=['NA', 'na'], low_memory=False)

def default_clf_scoring(train_file, test_file, path):
    train = read_csv(train_file)
    pipeline = get_clf_pipeline()
    pipeline.fit(train, train['loss'].values)
    
    test = read_csv(test_file)
    proba = pipeline.predict_proba(test)
    _, n = proba.shape
    columns = ['c' + str(i) for i in range(n)]
    proba_result = pd.DataFrame(proba, columns=columns)

    score = pipeline.score(test)
    score_result = pd.DataFrame(score, columns=columns)
    
    filepath = os.path.join(path, 'test_proba.csv')
    proba_result.to_csv(filepath, index=False)
    return pipeline

path = '/Users/satyajeetpal/spal/Princeton/Thesis/Data/'
filenames = ['train_v2.csv', 'test_v2.csv']
small_filenames = ['train_v2_small.csv', 'test_v2_small.csv']
train_file, test_file = [os.path.join(path, fname) for fname in filenames]
print 'training set is', train_file
print 'test set is', test_file
print 'default classifier training...'
clf = default_clf(train_file, test_file, path)
print 'loss regression training...'
reg = loss_reg(train_file, test_file, path)

def indices_max_n(array, n):
    a = np.copy(array)
    result = []
    for i in xrange(n):
        max_location = np.argmax(a)
        result.append(max_location)
        a[max_location] = -1
    return result


path = '/Users/satyajeetpal/spal/Princeton/Thesis/Data/'
filenames = ['train_v2.csv', 'test_v2.csv']
small_filenames = ['train_v2_small.csv', 'test_v2_small.csv']
train_file, test_file = [os.path.join(path, fname) for fname in filenames]
train = read_csv(train_file)
train.apply(np.random.permutation)
train_section = train[:10000]
test_section = train[10000:20000]

pipeline = train_predict.get_clf_pipeline()
pipeline.fit(train_section, train_section['loss'].values)

features, score = pipeline.score(test_section, test_section['loss'].values)
feature_names = np.array(['f528-f527', 'f528-f274', 'f527-f274', 'hasnull', 'f271', 'f2', 'f332', 'f776', 'f336', 'f777', 'f4', 'f5', 'f647', 'trial2', 'f337', 'f732', 'f380', 'f6', 'f597', 'f658', 'f731', 'f693', 'f465', 'f418', 'f73', 'f286', 'f382', 'f659', 'f24', 'f25', 'f16', 'f532', 'f725', 'f670', 'f287', 'f778', 'f674', 'f656', 'f403', 'f13', 'f657', 'f68', 'f699', 'f381', 'f1', 'f440', 'f738', 'f289', 'f82', 'f140', 'f66', 'f626', 'f378', 'f276', 'f183', 'f742', 'f278', 'f698', 'f280', 'f763', 'f771', 'f425', 'f664', 'f637', 'f392', 'f411', 'f660', 'f133', 'f621', 'f397', 'f590', 'f75', 'f593', 'f69', 'f49', 'f171', 'f348', 'f47', 'f178', 'f398', 'f744', 'f351', 'f409', 'f619', 'f391', 'f22', 'f161', 'f9', 'f438', 'f253', 'f535', 'f31', 'f146', 'f545', 'f168', 'f350', 'f514', 'f766', 'f600', 'f588', 'f334', 'f200', 'f79', 'f32', 'f385', 'f91', 'f250', 'f739', 'f672', 'f716', 'f199', 'f695', 'f609', 'f339', 'f358', 'f639', 'f313', 'f264', 'f330', 'f556', 'f756', 'f239', 'f640', 'f661', 'f26', 'f282', 'f525', 'f233', 'f112', 'f522', 'f281', 'f662', 'f734', 'f218', 'f437', 'f70', 'f375', 'f193', 'f638', 'f213', 'f202', 'f138', 'f324', 'f665', 'f27', 'f331', 'f201', 'f513', 'f598', 'f413', 'f410', 'f630', 'f57', 'f468', 'f407', 'f211', 'f365', 'f269', 'f342', 'f524', 'f14', 'f696', 'f596', 'f733', 'f223', 'f773', 'f746', 'f76', 'f631', 'f273', 'f368', 'f229', 'f356', 'f421', 'f181', 'f677', 'f222', 'f298', 'f649', 'f668', 'f231', 'f30', 'f151', 'f359', 'f275', 'f643', 'f390', 'f241', 'f740', 'f338', 'f144', 'f64', 'f248', 'f401', 'f154', 'f10', 'f102', 'f101', 'f308', 'f509', 'f760', 'f632', 'f422', 'f44', 'f416', 'f745', 'f343', 'f424', 'comb', 'f432', 'f751', 'f221', 'f673', 'f92', 'f142', 'f721', 'f479', 'f628', 'f614', 'f768', 'f651', 'f100', 'f153', 'f622', 'f143', 'f612', 'f212', 'f383', 'f458', 'f377', 'f230', 'f191', 'f238', 'f56', 'f208', 'f393', 'f620', 'f160', 'f3', 'f386', 'f402', 'f428', 'f404', 'f767', 'f652', 'f441', 'f279', 'f399', 'f357', 'f46', 'f217', 'f526', 'f305', 'f434', 'f373', 'f210', 'f169', 'f589', 'f45', 'f617', 'f591', 'f54', 'f179', 'f135', 'f180', 'f517', 'f277', 'f333', 'f442', 'f259', 'f676', 'f655', 'f292', 'f80', 'f645', 'f653', 'f586', 'f20', 'f21', 'f629', 'f426', 'f139', 'f141', 'f669', 'f775', 'f316', 'f261', 'f67', 'f349', 'f520', 'f448', 'f148', 'f300', 'f369', 'f566', 'f642', 'f654', 'f23', 'f774', 'f360', 'f353', 'f39', 'f646', 'f314', 'f471', 'f322', 'f395', 'f203', 'f663', 'f219', 'f529', 'f519', 'f594', 'f388', 'f364', 'f518', 'f71', 'f650', 'f352', 'f150', 'f249', 'f717', 'f228', 'f204', 'f405', 'f765', 'f15', 'f423', 'f680', 'f243', 'f671', 'f43', 'f636', 'f41', 'f344', 'f50', 'f624', 'f641', 'f366', 'f433', 'f240', 'f727', 'f121', 'f297', 'f111', 'comb1', 'f443', 'f51', 'f63', 'f623', 'f367', 'f60', 'f667', 'f149', 'f270', 'f394', 'f19', 'f755', 'f65', 'f254', 'f374', 'f136', 'f429', 'f61', 'f516', 'f361', 'f601', 'f29', 'f55', 'f611', 'f244', 'f216', 'f715', 'f436', 'comb2', 'f272', 'f689', 'f110', 'f194', 'f737', 'f451', 'f209', 'f679', 'f122', 'f132', 'f743', 'f618', 'f72', 'f648', 'f613', 'f444', 'f260', 'f533', 'f536', 'f81', 'f370', 'f431', 'f251', 'f306', 'f258', 'f523', 'f499', 'f40', 'f735', 'f376', 'f290', 'f263', 'f134', 'f384', 'f406', 'f170', 'f694', 'f220', 'f234', 'f147', 'f145', 'f205', 'f59', 'f634', 'f189', 'f430', 'f450', 'f587', 'f53', 'f412', 'f341', 'f159', 'f363', 'f726', 'f340', 'f288', 'f489', 'f17', 'f682', 'f635'])
indices = indices_max_n(features, 50)
print indices


plt.bar(np.arange(50), features[indices], align='center')
plt.xticks(np.arange(50), feature_names[indices], rotation=45)
plt.show()


score_result = pd.DataFrame(score, columns=columns)



