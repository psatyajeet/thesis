data = load_creditscoring2()
quantile = 0.95
sets = train_test_split(data.data, data.target, data.cost_mat, test_size=0.33, random_state=0)

X_train, X_test, y_train, y_test, cost_mat_train, cost_mat_test = sets

length = np.shape(X_train)[0]

current_cost = vector_subtract_alpha(cost_mat_train, alpha)
f = CostSensitiveLogisticRegression(solver='bfgs', random_state = None, max_iter=400, verbose=1)
f.fit(X_train, y_train, cost_mat_train)
y_pred_train_cslr = f.predict(X_train)

cost_loss(y_train, y_pred_train_cslr, cost_mat_train)

costs = find_costs(y_train, y_pred_train_cslr, cost_mat_train)

cost_df= pd.DataFrame(costs, columns = ['cost']) 




cVaR = alpha + (1/((1-quantile)*(length)))*np.sum(find_cost_over_var(y_train, y_pred_train_cslr, cost_mat_train, alpha))

VaR = cost_df.quantile(quantile) 


################

y_pred_train_lr = LogisticRegression(random_state=0).fit(X_train, y_train).predict(X_train)
cost_loss(y_train, y_pred_train_lr, cost_mat_train)
costs = find_costs(y_train, y_pred_train_lr, cost_mat_train)

################

f = RandomForestClassifier(random_state=0).fit(X_train, y_train)
y_prob_train = f.predict_proba(X_test)
y_pred_train_rf = f.predict(X_est)

f_bmr = BayesMinimumRiskClassifier()
f_bmr.fit(y_test, y_prob_test)
y_pred_test_bmr = f_bmr.predict(y_prob_test, cost_mat_test)

cost_loss(y_test, y_pred_test_bmr, cost_mat_test)
costs = find_costs(y_train, y_pred_train_bmr, cost_mat_train)


