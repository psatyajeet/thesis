%runs the exploration function with these set of parameters
%and returns 
%OC=the average opportunity cost over problem runs
%OC_SE=standard error for the average oc

function [OC,OC_SE]=UCB_Run_Project()

    N= 10; %number of runs to be made with different set of problems (or truths)
    M=[60]; %number of exploration steps for each run
    %M=10;
    T=(1:1);

    mu_0=[25; 20; 27; 22; 18; 29; 16; 18; 15; 17; 14; 15; 16; 13; 12; 9; 10; 11; 6; 8; 5]; 
    %mu_0:= a column vector that contains the prior means for each decision

    mean_house_prices=[110.5; 141.1; 152.4; 181.9; 186.6; 204.8; 209.8; 225; 245.7; 279; 279.1; 299; 303; 316.9; 326.9; 347.4; 362.6; 365.9; 399.5; 405.3; 407.6];
    
    sigma_0=[4; 2; 8; 6; 10; 6; 8; 5; 11; 12; 9; 10; 12; 11; 10; 4; 4; 3; 4; 6; 5];
    beta_0 = 1./(sigma_0.^2);
    
    %beta_0:= a column vector that contains the prior precisions

    sigma_W=[8; 4; 6; 8; 7; 7; 8; 6; 7; 7; 6; 8; 6; 7; 8; 8; 8; 6; 8; 5; 4];
    beta_W = 1./(sigma_W.^2);
    %beta_W:= precision (1/variance) of the error from observations, for each decision
    
    KGonline_OCs=zeros(length(T), 1);
    KGonline_OCSDs=zeros(length(T), 1);
    KGonline_1_OCs=zeros(length(T), 1);
    KGonline_1_OCSDs=zeros(length(T), 1);
    KGonline_2_OCs=zeros(length(T), 1);
    KGonline_2_OCSDs=zeros(length(T), 1);

    
    for j=M
        
        for i=T

            [OC,OC_SE, choices1]=kgOnline(mean_house_prices, mu_0,beta_0,beta_W,N,j, 1);
            KGonline_OCs(i) = OC;
            KGonline_OCSDs(i) = OC_SE;
            [OC,OC_SE, choices2]=kgOnline(mean_house_prices, mu_0,beta_0,beta_W,N,j, 0.98);
            KGonline_1_OCs(i) = OC;
            KGonline_1_OCSDs(i) = OC_SE;
            [OC,OC_SE, choices3]=kgOnline(mean_house_prices, mu_0,beta_0,beta_W,N,j, 0.95);
            KGonline_2_OCs(i) = OC;
            KGonline_2_OCSDs(i) = OC_SE;
            [OC,OC_SE, choices4]=kgOnline(mean_house_prices, mu_0,beta_0,beta_W,N,j, 0.90);
            KGonline_3_OCs(i) = OC;
            KGonline_3_OCSDs(i) = OC_SE;
            
            figure(1)
            axis([0 M+10 0 22]);
            hold all
            for k=(1:N)
                disp(choices1);
                plot(choices1(k, (1:M)));
            end
            
            figure(2)
            axis([0 M+10 0 22]);
            hold all
            for k=(1:N)
                disp(choices2);
                plot(choices2(k, (1:M)));
            end
            
            figure(3)
            axis([0 M+10 0 22]);
            hold all
            for k=(1:N)
                disp(choices3);
                plot(choices3(k, (1:M)));
            end

            figure(4)
            axis([0 M+10 0 22]);
            hold all
            for k=(1:N)
                disp(choices4);
                plot(choices4(k, (1:M)));
            end
        end

        figure(5)
        hold all
        errorbar(KGonline_OCs, KGonline_OCSDs);
        errorbar(KGonline_1_OCs, KGonline_1_OCSDs);
        errorbar(KGonline_2_OCs, KGonline_2_OCSDs);


    end
    legend('KG', 'KG w/ 0.98 ', 'KG w/ 0.95');

end