%runs the exploration function with these set of parameters
%and returns 
%OC=the average opportunity cost over problem runs
%OC_SE=standard error for the average oc

function [OC,OC_SE]=kgRunProject_DifferentPrices()

    N= 40; %number of runs to be made with different set of problems (or truths)
    M=(15); %number of exploration steps for each run
    %M=10;
    T=(1:30);

    mu_0=[25; 20; 27; 22; 18; 29; 16; 18; 15; 17; 14; 15; 16; 13; 12; 9; 10; 11; 6; 8; 5]; 
    %mu_0:= a column vector that contains the prior means for each decision


    mean_house_prices=[60; 80; 100; 120; 140; 160; 170; 180; 200; 210; 220; 230; 240; 250; 260; 280; 300; 320; 340; 360; 380];
    low_end = mean_house_prices.*0.6;
    high_end = mean_house_prices./0.6;
       
    low_mu_0 = mu_0/0.6;
    high_mu_0 = mu_0*0.6;
    
    sigma_0=[1; 1; 2; 1; 1; 2; 1; 2; 3; 1; 3; 1; 2; 1; 4; 5; 3; 2; 4; 3; 2];
    beta_0 = 1./(sigma_0.^2);
    
    %beta_0:= a column vector that contains the prior precisions

    sigma_W=[1; 1; 2; 1; 1; 2; 1; 2; 3; 1; 3; 1; 2; 1; 4; 5; 3; 2; 4; 3; 2];
    low_sigma_W=sigma_W.*0.4;
    high_sigma_W=sigma_W./0.4;
    
    beta_W = 1./(sigma_W.^2);
    low_beta_W=1./(low_sigma_W.^2);
    high_beta_W=1./(high_sigma_W.^2);
    %beta_W:= precision (1/variance) of the error from observations, for each decision
    

    low_KG_OCs=zeros(length(T), 1);
    low_KG_OCSDs=zeros(length(T), 1);
    high_KG_OCs=zeros(length(T), 1);
    high_KG_OCSDs=zeros(length(T), 1);

    low_value_of_info = zeros(length(M)); 
    high_value_of_info = zeros(length(M)); 
    
    for j=M
        
        low = zeros(length(T));
        high = zeros(length(T));
        for i=T
            [OC,OC_SE, low(i)]=kg_withVOI(low_end, low_mu_0,beta_0,low_beta_W,N,j);
            low_KG_OCs(i) = OC;
            low_KG_OCSDs(i) = OC_SE;
            [OC,OC_SE, high(i)]=kg_withVOI(high_end, high_mu_0,beta_0,high_beta_W,N,j);
            high_KG_OCs(i) = OC;
            high_KG_OCSDs(i) = OC_SE;
        end
        hold all
        errorbar(low_KG_OCs, low_KG_OCSDs);
        errorbar(high_KG_OCs, high_KG_OCSDs);

        low_value_of_info(j)=mean(low(1));
        high_value_of_info(j)=mean(high(1));
    end
    legend('KG-low prices', 'KG-high prices');
    
   

end