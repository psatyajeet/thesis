%runs the exploration function with these set of parameters
%and returns
%OC=the average opportunity cost over problem runs
%OC_SE=standard error for the average oc

function [OC,OC_SE]=kg_RunProject()

N= 50; %number of runs to be made with different set of problems (or truths)
M=[15]; %number of exploration steps for each run
%M=10;
T=(1:30);

mu_0=[25; 20; 27; 22; 18; 29; 16; 18; 15; 17; 14; 15; 16; 13; 12; 9; 10; 11; 6; 8; 5];
%mu_0:= a column vector that contains the prior means for each decision

mean_house_prices=[110.5; 141.1; 152.4; 181.9; 186.6; 204.8; 209.8; 225; 245.7; 279; 279.1; 299; 303; 316.9; 326.9; 347.4; 362.6; 365.9; 399.5; 405.3; 407.6];

sigma_0=[4; 2; 8; 6; 10; 6; 8; 5; 11; 12; 9; 10; 12; 11; 10; 4; 4; 3; 4; 6; 5];
beta_0 = 1./(sigma_0.^2);

%beta_0:= a column vector that contains the prior precisions

sigma_W=[8; 4; 6; 8; 7; 7; 8; 6; 7; 7; 6; 8; 6; 7; 8; 8; 8; 6; 8; 5; 4];
beta_W = 1./(sigma_W.^2);
%beta_W:= precision (1/variance) of the error from observations, for each decision

explore_choice=zeros(length(T), 1);
explore_est=zeros(length(T), 1);
exploit_choice=zeros(length(T), 1);
exploit_est=zeros(length(T), 1);
ie_choice=zeros(length(T), 1);
ie_est=zeros(length(T), 1);
boltzmann_choice=zeros(length(T), 1);
boltzmann_est=zeros(length(T), 1);
KG_choice=zeros(length(T), 1);
KG_est=zeros(length(T), 1);

KG_OCs=zeros(length(T), 1);
KG_OCSDs=zeros(length(T), 1);
explore_OCs=zeros(length(T), 1);
explore_OCSDs=zeros(length(T), 1);
exploit_OCs=zeros(length(T), 1);
exploit_OCSDs=zeros(length(T), 1);
ie_OCs=zeros(length(T), 1);
ie_OCSDs=zeros(length(T), 1);
boltzmann_OCs=zeros(length(T), 1);
boltzmann_OCSDs=zeros(length(T), 1);
for j=M
    
    for i=T
        [OC,OC_SE, KG_choice(i), KG_est(i)]=kg(mean_house_prices, mu_0,beta_0,beta_W,N,j);
        KG_OCs(i) = OC;
        KG_OCSDs(i) = OC_SE;
        [OC,OC_SE, explore_choice(i), explore_est(i)]=exploration(mean_house_prices, mu_0,beta_0,beta_W,N,j);
        explore_OCs(i) = OC;
        explore_OCSDs(i) = OC_SE;
        [OC,OC_SE, exploit_choice(i), exploit_est(i)]=exploitation(mean_house_prices, mu_0,beta_0,beta_W,N,j);
        exploit_OCs(i) = OC;
        exploit_OCSDs(i) = OC_SE;
        [OC,OC_SE, ie_choice(i), ie_est(i)]=ie(mean_house_prices, mu_0,beta_0,beta_W,N,j, 2);
        ie_OCs(i) = OC;
        ie_OCSDs(i) = OC_SE;
        [OC, OC_SE, boltzmann_choice(i), boltzmann_est(i)]=boltzmann(mean_house_prices, mu_0, beta_0, beta_W, N, j, 0.08);
        boltzmann_OCs(i) = OC;
        boltzmann_OCSDs(i) = OC_SE;
    end
    
    disp(mode(explore_choice));
    disp(mean(explore_est));
    disp(mean(explore_OCs));
    disp(mean(explore_OCSDs));
    
    disp(mode(exploit_choice));
    disp(mean(exploit_est));
    disp(mean(exploit_OCs));
    disp(mean(exploit_OCSDs));
    
    disp(mode(ie_choice));
    disp(mean(ie_est));
    disp(mean(ie_OCs));
    disp(mean(ie_OCSDs));
    
    disp(mode(boltzmann_choice));
    disp(mean(boltzmann_est));
    disp(mean(boltzmann_OCs));
    disp(mean(boltzmann_OCSDs));
    
    disp(mode(KG_choice));
    disp(mean(KG_est));
    disp(mean(KG_OCs));
    disp(mean(KG_OCSDs));
    
    figure(1);
    errorbar(KG_OCs, KG_OCSDs);
    legend('KG');
    xlabel('Trial');
    ylabel('Opportunity Cost ($)');
    figure(2);
    errorbar(explore_OCs, explore_OCSDs);
    legend('Exploration');
    xlabel('Trial');
    ylabel('Opportunity Cost ($)');
    figure(3);
    errorbar(exploit_OCs, exploit_OCSDs);
    legend('Exploitation');
    xlabel('Trial');
    ylabel('Opportunity Cost ($)');
    figure(4);
    errorbar(ie_OCs, ie_OCSDs);
    legend('IE');
    xlabel('Trial');
    ylabel('Opportunity Cost ($)');
    figure(5);
    errorbar(boltzmann_OCs, boltzmann_OCSDs);
    legend('Boltzmann');
    xlabel('Trial');
    ylabel('Opportunity Cost ($)');
end
%legend('KGCB', 'KG', 'Exploration', 'Exploitation', 'IE', 'Boltzmann');
%legend('KGCB', 'KG', 'IE');

end