%This function takes in:
%mu_0:= a column vector that contains the prior means for each decision
%beta_0:= a column vector that contains the prior precisions
%beta_W:= variance of the error from observations, for each decision
%N:= number of runs to be made with different set of problems (or truths)
%M:= number of exploration steps for each run

%It produces
%average_result:= the average opportunity loss over runs
%se_result:= the standard error for the average

function [average_result,se_result]=boltzmann(prices, mu_0,beta_0,beta_W,N,M,tuneable_parameter)

K=length(mu_0); %number of available choices
average_result=0; %this will be our average opportunity cost over runs
variance_result=0; %and this is the standard error for this variable
for n=1:N %go through each run, for each run the problem (the set of truths) will be different
    
    mu_z=randn(K,1); %the true z values for each decision
    mu=mu_z.*1./sqrt(beta_0)+mu_0; %txhe true values for each decision (given by Z*Sigma+Mean, where Sigma=(1/b)^(0.5))
    
    mu_est=mu_0; %our estimates for the mean, we start with the prior
    beta_est=beta_0; %our precision for the mean
    
    for k=1:M %try the exploration for M number of times
        
        this_max=max(mu_est);
        p=exp(tuneable_parameter .* (mu_est-this_max));
        total_p=sum(p);
        p=p ./ total_p;    
        
        % Pick a decision using Boltzmann
        random=rand(1);
        i=1;
        value=p(i);
        while value<1
           if random <= value
               x=i;
               break;
           end
           i=i+1;
           value=value+p(i);
        end
        
        %Pick a random decision (where each is given an equal probability)
        % x=ceil(K*rand(1));
        %ceil is the ceiling function, it takes a number and rounds it to the closest bigger integer
        %ceil(A*rand(1)) generates a discrete uniform random variable [1,A]
        
        %observe the outcome of the decision
        %W_k=mu_k+Z*SigmaW_k where SigmaW is standard deviation of the
        %error for each observation
        W_k=mu(x)+randn(1)*1./sqrt(beta_W(x));
        
        %update the mean and precision for this decision by the Bayes
        %updating rules for Normal-Normal distributions
        mu_est(x)=(beta_est(x)*mu_est(x)+beta_W(x)*W_k)/(beta_est(x)+beta_W(x));
        beta_est(x)=beta_est(x)+beta_W(x);
        
    end
    
    [max_est, max_choice]=max(mu_est.*prices); 
    [max_actual, max_actual_choice] = max(mu.*prices);
    
    %max_est=max(mu_est) is the best estimated value
    %max_choice=argmax(mu_est) is the decision which we believe to be the
    %best
    
    o_cost=mu(max_actual_choice)*prices(max_actual_choice)-mu(max_choice)*prices(max_choice);
    %the opportunity cost of going with our decision compared to the best
    %for this particular run n
    
    %now update the variance and the average recursively
    %for formulas see (2.3), (2.4) on page 24 of the book
    if n>2
        variance_result=(n-2)/(n-1)*variance_result+(1/n)*(o_cost-average_result)^2;
    else
        if n==1
            variance_result=0;
        else
            variance_result=0.5*(o_cost-average_result)^2;
        end
    end
    
    average_result=(1-1/n)*average_result+(1/n)*o_cost;
end

se_result=sqrt(variance_result/N); %calculate standard error
conf_interval = [average_result - 1.96 * se_result , average_result + 1.96 * se_result];
%fprintf( 'The average opportunity cost is %4.4f with a standard error of %4.4f \n', average_result, se_result);
%fprintf('The confidence interval is (%4.4f , %4.4f) \n', conf_interval(1), conf_interval(2));
end