%runs the exploration function with these set of parameters
%and returns 
%OC=the average opportunity cost over problem runs
%OC_SE=standard error for the average oc

function [OC,OC_SE]=kgRun()

    N= 50; %number of runs to be made with different set of problems (or truths)
    M=[10]; %number of exploration steps for each run
    %M=10;
    T=(1:30);

    mu_0=[25; 20; 27; 22; 18; 29; 16; 18; 15; 17; 14; 15; 16; 13; 12; 9; 10; 11; 6; 8; 5]; 
    %mu_0:= a column vector that contains the prior means for each decision

    mean_house_prices=[110.5; 141.1; 152.4; 181.9; 186.6; 204.8; 209.8; 225; 245.7; 279; 279.1; 299; 303; 316.9; 326.9; 347.4; 362.6; 365.9; 399.5; 405.3; 407.6];
    
    sigma_0=[4; 2; 8; 6; 10; 6; 8; 5; 11; 12; 9; 10; 12; 11; 10; 4; 4; 3; 4; 6; 5];
    beta_0 = 1./(sigma_0.^2);
    
    %beta_0:= a column vector that contains the prior precisions

    sigma_W=[8; 4; 6; 8; 7; 7; 8; 6; 7; 7; 6; 8; 6; 7; 8; 8; 8; 6; 8; 5; 4];
    beta_W = 1./(sigma_W.^2);
    %beta_W:= precision (1/variance) of the error from observations, for each decision
    

    KG_OCs=zeros(length(T), 1);
    KG_OCSDs=zeros(length(T), 1);
    explore_OCs=zeros(length(T), 1);
    explore_OCSDs=zeros(length(T), 1);
    exploit_OCs=zeros(length(T), 1);
    exploit_OCSDs=zeros(length(T), 1);
    ie_OCs=zeros(length(T), 1);
    ie_OCSDs=zeros(length(T), 1);
    boltzmann_OCs=zeros(length(T), 1);
    boltzmann_OCSDs=zeros(length(T), 1);
    for j=M
        
        for i=T
            [OC,OC_SE]=kg(mean_house_prices, mu_0,beta_0,beta_W,N,j);
            KG_OCs(i) = OC;
            KG_OCSDs(i) = OC_SE;
            [OC,OC_SE]=exploration(mean_house_prices, mu_0,beta_0,beta_W,N,j);
            explore_OCs(i) = OC;
            explore_OCSDs(i) = OC_SE;
            [OC,OC_SE]=exploitation(mean_house_prices, mu_0,beta_0,beta_W,N,j);
            exploit_OCs(i) = OC;
            exploit_OCSDs(i) = OC_SE;
            [OC,OC_SE]=ie(mean_house_prices, mu_0,beta_0,beta_W,N,j, 2);
            ie_OCs(i) = OC;
            ie_OCSDs(i) = OC_SE;
            [OC, OC_SE]=boltzmann(mean_house_prices, mu_0, beta_0, beta_W, N, j, 0.08);
            boltzmann_OCs(i) = OC;
            boltzmann_OCSDs(i) = OC_SE;
        end
        hold all
        errorbar(KG_OCs, KG_OCSDs);
        errorbar(explore_OCs, explore_OCSDs);
        errorbar(exploit_OCs, exploit_OCSDs);
        errorbar(ie_OCs, ie_OCSDs);
        errorbar(boltzmann_OCs, boltzmann_OCSDs);
    end
    legend('KGCB', 'KG', 'Exploration', 'Exploitation', 'IE', 'Boltzmann');

end