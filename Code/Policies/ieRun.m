%runs the exploration function with these set of parameters
%and returns
%OC=the average opportunity cost over problem runs
%OC_SE=standard error for the average oc

function [OC,OC_SE]=ieRun()

N= 5000; %number of runs to be made with different set of problems (or truths)
M= 50; %number of exploration steps for each run
% N = 1;
% M = 1;

mu_0=[75;80;85;95;100;105;115;120;125;100];
%mu_0:= a column vector that contains the prior means for each decision

beta_0=[0.02;0.01;0.007;0.02;0.01;0.007;0.02;0.01;0.007;0.005];
%beta_0:= a column vector that contains the prior precisions

beta_W=[0.05;0.025;0.0175;0.05;0.025;0.0175;0.05;0.025;0.0175;0.0125];
%beta_W:= precision (1/variance) of the error from observations, for each decision

% zalpha = 0:5;
% zalpha = (4:.25:5);
 zalpha = 4.25;
for i = zalpha
    fprintf( 'We are using zalpha=%4.4f \n', i);
    %for j=1:10
    [OC,OC_SE]=ie(mu_0,beta_0,beta_W,N,M,i);
    %end
end



end