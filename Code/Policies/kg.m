%This function takes in:
%mu_0:= a column vector that contains the prior means for each decision
%beta_0:= a column vector that contains the prior precisions
%beta_W:= variance of the error from observations, for each decision
%N:= number of runs to be made with different set of problems (or truths)
%M:= number of exploration steps for each run

%It produces
%average_result:= the average opportunity loss over runs
%se_result:= the standard error for the average

function [average_result,se_result]=kg(prices, mu_0,beta_0,beta_W,N,M)

K=length(mu_0); %number of available choices
average_result=0; %this will be our average opportunity cost over runs
variance_result=0; %and this is the standard error for this variable
zero_cost=0; %this is the number of times the opportunity cost is 0. 


for n=1:N %go through each run, for each run the problem (the set of truths) will be different
    
    mu_z=randn(K,1); %the true z values for each decision
    mu=mu_z.*1./sqrt(beta_0)+mu_0; %the true values for each decision (given by Z*Sigma+Mean, where Sigma=(1/b)^(0.5))
    %mu=(1:21);
    mu_0=(21:-1:1);
    mu_0=(exp(-0.02.*mu_0).*mu_0)';
    
    mu_est=mu_0; %our estimates for the mean, we start with the prior
    beta_est=beta_0; %our precision for the mean
    
    for k=1:M %try the exploration for M number of times
        sigma_est=zeros(K);
        zeta=zeros(K);
        f_zeta=zeros(K);
        knowledge_gradient=zeros(K);
        kg_with_prices=zeros(K);
        for j=1:K
            % sigma_est = sqrt(1 ./ beta_est);
            sigma_est(j) = sqrt(1/beta_est(j) - 1/(1+beta_est(j)));
            b = mu_est;
            b(j) = [];
            maximum = max(b);
            zeta(j) = -abs(((mu_est(j) - maximum)/sigma_est(j)));
            f_zeta(j) = zeta(j) * normcdf(zeta(j)) + normpdf(zeta(j));
            knowledge_gradient(j) = f_zeta(j) * sigma_est(j);
            kg_with_prices(j) = knowledge_gradient(j)*prices(j);
        end
        
        
        
        [the_max, x] = max(kg_with_prices);
        x=x(1);
        %Pick a random decision (where each is given an equal probability)
        %x=ceil(K*rand(1)); 
        %ceil is the ceiling function, it takes a number and rounds it to the closest bigger integer
        %ceil(A*rand(1)) generates a discrete uniform random variable [1,A]
        
        %observe the outcome of the decision
        %W_k=mu_k+Z*SigmaW_k where SigmaW is standard deviation of the
        %error for each observation
        W_k=mu(x)+randn(1)*1./sqrt(beta_W(x));
        
        %update the mean and precision for this decision by the Bayes
        %updating rules for Normal-Normal distributions
        mu_est(x)=(beta_est(x)*mu_est(x)+beta_W(x)*W_k)/(beta_est(x)+beta_W(x));
        beta_est(x)=beta_est(x)+beta_W(x);
        
        
    end
    [max_est, max_choice]=max(mu_est.*prices); 
    [max_actual, max_actual_choice] = max(mu.*prices);
    
    %max_est=max(mu_est) is the best estimated value
    %max_choice=argmax(mu_est) is the decision which we believe to be the
    %best
    o_cost=mu(max_actual_choice)*prices(max_actual_choice)-mu(max_choice)*prices(max_choice);
    
    %the opportunity cost of going with our decision compared to the best
    %for this particular run n
    
    %now update the variance and the average recursively
    %for formulas see (2.3), (2.4) on page 24 of the book
    if n>2        
        variance_result=(n-2)/(n-1)*variance_result+(1/n)*(o_cost-average_result)^2;
    else
        if n==1
            variance_result=0;
        else
            variance_result=0.5*(o_cost-average_result)^2;
        end
    end
    if (o_cost == 0)
        zero_cost=zero_cost+1;
    end
    average_result=(1-1/n)*average_result+(1/n)*o_cost;
end
    
se_result=sqrt(variance_result/N); %calculate standard error
%fprintf( 'The average opportunity cost is %4.4f with a standard error of %4.4f \n', average_result, se_result);
%fprintf( 'The number of times that the policy found the best alternative is %4f \n', zero_cost);
end